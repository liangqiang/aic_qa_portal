import refile
import pandas as pd
path='s3://wanghongyuan/onyx_voice/'
df=pd.DataFrame()
# print(len(refile.smart_listdir(path)))
prefix_name_list,suffix_name_list=[],[]
for root,dirs,files in refile.smart_walk(path):
    for file in files:
        if file.endswith('.mp3') and "_" in file:
            # 查找第一个"_"的位置
            first_index = file.find('_')

            # 查找第二个"_"的位置，从第一个"_"的后面开始查找
            second_index = file.find('_', first_index + 1)
            prefix_name=file[:second_index]
            suffix_name=file[second_index+1:] 
            #df[prefix_name]=suffix_name.replace('.mp3', '')
            prefix_name_list.append(prefix_name)
            suffix_name_list.append(suffix_name.replace('.mp3', ''))
            # print(prefix_name)
            # print(suffix_name)
        else:
            print(file)
        #print(file)
df["prefix_name"]=prefix_name_list
df["suffix_name"]=suffix_name_list
df['NumericPart'] = df['prefix_name'].str.extract('(\d+)', expand=False).astype(int)
# 按照 NumericPart 列的值进行升序排序
df = df.sort_values(by='NumericPart')
df = df.drop(columns=['NumericPart'])
# 按照 NumericPart 列的值进行升序排序
# df['NumericPart'] = df['prefix_name'].str.extract('(\d+)', expand=False)
# # 将NumericPart列转换为整数，然后按照数字大小进行升序排序
# df_sorted = df.copy()
# df_sorted['NumericPart'] = df_sorted['NumericPart'].astype(int)
# df_sorted.sort_values(by='NumericPart', inplace=True)
# df_sorted.drop(columns=['NumericPart'], inplace=True)
df.to_excel("test.xlsx",index=False)