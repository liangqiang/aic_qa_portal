from utils.general import render_ag_grid,download_file,audio_process
import streamlit as st
import pandas as pd
import refile
import os,shutil
from datetime import datetime
now = datetime.now().strftime("%Y%m%d")

def read_csv(csv_file):
    df=pd.read_csv(csv_file,encoding='utf-8')
    return df['wav'],df['txt'],df['tag']
    #return df['wav'],df['txt']

def read_audio_file(path):
    
    wav_list,txt_list=[],[]
    for root,dir,files in os.walk(path):
        for file in files:
            if file.endswith('.mp3') or file.endswith('.wav') or file.endswith('.m4a') :
                wav_list.append(os.path.join(root,file))
            elif '.txt' in file:
                txt_list.append(os.path.join(root,file))
    wav_list=sorted(wav_list)
    txt_list=sorted(txt_list)
    return wav_list,txt_list

def amend_txt(path,txt):
    with refile.smart_open(path,'w',encoding='utf-8') as w:
        w.write(txt)
        


if '__main__' ==__name__:
    st.set_page_config(layout="wide",initial_sidebar_state ='collapsed')
    if not os.path.exists('data/output/audio_release/'):
        os.makedirs('data/output/audio_release/')
    csv_path=r"/data/QA/GIT/aic_qa_portal/data/output/play_audio"
    csv_list=os.listdir(csv_path)
    one_col=st.columns([2,4,1])
    df=pd.DataFrame()
    
    with one_col[0],one_col[1],one_col[2]:
        csv_file=one_col[0].selectbox("选择CSV文件",csv_list)
        audio_path=one_col[1].text_input('待处理音频路径','')
        save_name=csv_file.replace('.csv', '')
        timestamp_path=f'data/output/audio_release/{now}_save_name_timestamp'
        tone_path=f'data/output/audio_release/{now}_save_name_tone'
        other_path=f'data/output/audio_release/{now}_save_name_other'
        mkdir_file_list=[timestamp_path,tone_path,other_path]
        for i in mkdir_file_list:
            if not os.path.exists(i):
                os.makedirs(i)
        one_col[2].title('')
        start_button=one_col[2].button("Start")
        if start_button:
            wav_list_process,txt_list_process,tag_list_process=read_csv(os.path.join(csv_path,csv_file))
            audio_list,txt_list=[],[]
            #all_list=os.listdir(audio_path)
            for root,dir,files in refile.smart_walk(audio_path):
                for file in files:
                    if file.endswith('.mp3') or file.endswith('.wav') or file.endswith('.m4a'):
                        #audio_file=refile.smart_path_join(root,file)
                        #txt_file=audio_file.replace('.'+file.rsplit('.', 1)[-1],'.txt')
                        audio_list.append(file)
                        txt_list.append(file.replace('.'+file.rsplit('.', 1)[-1],'.txt'))
            audio_list=sorted(audio_list)
            print(audio_list)
            txt_list=sorted(txt_list)
            dict_data={k:v for k,v  in zip(audio_list,txt_list)}
            wav_list_process=[ i.split('/')[-1] for i in wav_list_process ]
            wav_list_,txt_list_,wav_list_path,txt_list_path,tag_list_=[],[],[],[],[]
            for wav,txt ,tag in zip(wav_list_process,txt_list_process,tag_list_process):
                if wav in dict_data:
                    if tag=='删除':
                        os.remove(os.path.join(audio_path,wav))
                        os.remove(os.path.join(audio_path,dict_data[wav]))
                    elif tag=='修改':
                        amend_txt(os.path.join(audio_path,dict_data[wav]),txt)
                    elif tag=='语气词':
                        shutil.move(os.path.join(audio_path,wav),os.path.join(tone_path,wav))
                        amend_txt(os.path.join(tone_path,dict_data[wav]),txt)
                        os.remove(os.path.join(audio_path,dict_data[wav]))
                    elif tag=='空白音':
                        shutil.move(os.path.join(audio_path,wav),os.path.join(timestamp_path,wav))
                        #shutil.copy(os.path.join(audio_path,dict_data[wav]),os.path.join(timestamp_path,dict_data[wav]))
                        amend_txt(os.path.join(timestamp_path,dict_data[wav]),txt)
                        os.remove(os.path.join(audio_path,dict_data[wav]))
                    elif tag=='其他':
                        shutil.move(os.path.join(audio_path,wav),os.path.join(other_path,wav))
                        amend_txt(os.path.join(other_path,dict_data[wav]),txt)
                        os.remove(os.path.join(audio_path,dict_data[wav]))
                    else:
                        print(f"{wav}找不到")
                    wav_list_.append(wav)
                    txt_list_.append(txt)
                    txt_list_path.append(os.path.join(audio_path,dict_data[wav]))
                    wav_list_path.append(os.path.join(audio_path,wav))
                    tag_list_.append(tag)
                    print(f"{tag}:{wav}")
                    print(f"{tag}:{dict_data[wav]}")
                    print('---'*50)
            df['wav']=wav_list_
            df['tag']=tag_list_
            df['amend']=txt_list_
            df['wav_path']=wav_list_path
            df['txt_path']=txt_list_path
    st.dataframe(df,use_container_width=True)
    df.to_csv(f'data/output/audio_release/info_{csv_file}')
