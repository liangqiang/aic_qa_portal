access_key_id = 'AKLTMTYwNTUwNmJjYWM0NDUwY2FiOTBmZTk0ODY5ZjEzODY'
secret_access_key = 'TURKaU5HSmhPV1ZpTlRjeE5HVTFNV0kyT0RKaU16WTFabVZpTWpRM1lqRQ=='
import re
import tempfile
import tos
import os
import json
import streamlit as st
import pandas as pd
import random
import subprocess
import shlex
import concurrent.futures

from tqdm import tqdm
from utils.general import render_ag_grid,download_file,get_dataframe_tar_from_oss,get_unquote
#subprocess.run(["source", "~/.bash_profile"], shell=True)
with open("/data/liangqiang/QA/aic_qa_portal/env/proxy_config.txt", "r") as file:
    data = file.read()

if data:
    proxy_config=json.loads(data)
    os.environ['all_proxy'] = proxy_config['all_proxy']
    os.environ['no_proxy'] = proxy_config['no_proxy']
    os.environ['http_proxy'] = proxy_config['http_proxy']
    os.environ['https_proxy'] = proxy_config['https_proxy']
# 从环境变量获取 AKSK 信息
ak = os.getenv("TOS_ACCESS_KEY")
sk = os.getenv("TOS_SECRET_KEY")
# your endpoint 和 your region 填写Bucket 所在区域对应的Endpoint。# 以华北2(北京)为例，your endpoint 填写 tos-cn-beijing.volces.com，your region 填写 cn-beijing。
endpoint = "https://tos-cn-shanghai.volces.com"
region = "cn-shanghai"
# print(ak)
# client = tos.TosClientV2(access_key_id, secret_access_key, endpoint, region)
client = tos.TosClientV2(access_key_id, secret_access_key, endpoint, region)
# your endpoint 和 your region 填写Bucket 所在区域对应的Endpoint。# 以华北2(北京)为例，your endpoint 填写 tos-cn-beijing.volces.com，your region 填写 cn-beijing。
bucket_name = "a-collections-sh"


def audio_dur_from_binary(bucket_name,wav_path):
    data=read_file(bucket_name,wav_path)
    with tempfile.NamedTemporaryFile(delete=False,dir='/data/tmp/audio_tmp') as temp_input_file:
        temp_input_file.write(data)
        audio_data = temp_input_file.name
    command = f'ffmpeg -i {shlex.quote(audio_data)} 2>&1 | grep Duration'
    try:
        result = subprocess.run (command, shell=True,  text=True, check=True,stdout=subprocess.PIPE)
        duration_match = re.search(r'Duration: (\d+):(\d+):(\d+\.\d+)', result.stdout)
        hours, minutes, seconds = map(float, duration_match.groups())
        duration_seconds = hours * 3600 + minutes * 60 + seconds
        return duration_seconds
    except Exception as e:
        print(e)
        return None
    finally:
        os.remove(audio_data)
def get_all_audio(bucket_name,object_key):
    try:
        data=[]
        # 创建 TosClientV2 对象，对桶和对象的操作都通过 TosClientV2 实现
        # 列举指定桶下特定前缀所有对象
        truncated = True
        continuation_token = ''

        while truncated:
            result = client.list_objects_type2(bucket_name, prefix=object_key, continuation_token=continuation_token)
            for index, iterm in enumerate(result.contents):
                file_name=iterm.key
                if file_name.endswith('.mp3') or file_name.endswith('.m4a') or file_name.endswith('.wav'):
                    #(f"{index}:{file_name}")
                    data.append(file_name)
            truncated = result.is_truncated
            continuation_token = result.next_continuation_token
            
        return data
    except tos.exceptions.TosClientError as e:
        # 操作失败，捕获客户端异常，一般情况为非法请求参数或网络异常
        print('fail with client error, message:{}, cause: {}'.format(e.message, e.cause))
    except tos.exceptions.TosServerError as e:
        # 操作失败，捕获服务端异常，可从返回信息中获取详细错误信息
        print('fail with server error, code: {}'.format(e.code))
        # request id 可定位具体问题，强烈建议日志中保存
        print('error with request id: {}'.format(e.request_id))
        print('error with message: {}'.format(e.message))
        print('error with http code: {}'.format(e.status_code))
        print('error with ec: {}'.format(e.ec))
        print('error with request url: {}'.format(e.request_url))
    except Exception as e:
        print('fail with unknown error: {}'.format(e))
def read_file(bucket_name,object_key):
    #print(bucket_name)
    try:
        object_stream = client.get_object(bucket_name, object_key)
        data=object_stream.read()
        #print(data)
        return data
    # except tos.exceptions.TosClientError as e:
    #     # 操作失败，捕获客户端异常，一般情况为非法请求参数或网络异常
    #     print('fail with client error, message:{}, cause: {}'.format(e.message, e.cause))
    # except tos.exceptions.TosServerError as e:
    #     # 操作失败，捕获服务端异常，可从返回信息中获取详细错误信息
    #     print('fail with server error, code: {}'.format(e.code))
    #     # request id 可定位具体问题，强烈建议日志中保存
    #     print('error with request id: {}'.format(e.request_id))
    #     print('error with message: {}'.format(e.message))
    #     print('error with http code: {}'.format(e.status_code))
    #     #print('error with ec: {}'.format(e.ec))
    #     print('error with request url: {}'.format(e.request_url))
    except Exception as e:
        print('fail with unknown error: {}'.format(e))
        return None
        
def audio_files_generator(all_task):
    for task in all_task:
        yield task
if '__main__' == __name__:
    st.set_page_config(layout="wide", initial_sidebar_state='collapsed')
    one_col = st.columns([1,7,1,1,1])
    two_col = st.columns([1, 1])
    three_col = st.columns([1])
    df = pd.DataFrame()
    df_err = pd.DataFrame()

    with one_col[0],one_col[1],one_col[2],one_col[3],one_col[4],two_col[0]:

        tos_path_input = one_col[1].text_input('TOS 地址', 'https://console.volcengine.com/tos/bucket/setting?id=a-collections-sh&region=cn-shanghai&dirPrefix=data%2F%E7%94%9F%E6%B4%BB%E7%A4%BE%E5%8C%BA%E7%B1%BB%2Fbilibili%2Fbilibili-20231124%2F%E8%AE%B8%E5%90%89%E5%A6%82%2F%E8%B4%A6%E5%8F%B7%E5%B7%B2%E6%B3%A8%E9%94%80_3494380306434183%2F30%20%E7%AC%AC29%E8%AF%BE%20_%20%E7%88%B6%E6%AF%8D%E8%AF%BE%E5%A0%82%EF%BC%9A%E7%88%B6%E6%AF%8D%E5%A6%82%E4%BD%95%E5%8A%A9%E5%AD%A9%E5%AD%90%E4%B8%80%E8%87%82%E4%B9%8B%E5%8A%9B%EF%BC%9F%2F&type=objects').lstrip("/").rstrip('/')
        
        tos_path = get_unquote(tos_path_input)
        if 'https://' in tos_path:
            bucket_name=tos_path.split('id=')[-1].split('&')[0]
            tos_path=tos_path.split("dirPrefix=")[-1].replace("/&type=objects", "")+'/'
            
            
        else:
            bucket_name=tos_path.split('//')[-1].split('/')[0]
            tos_path=tos_path.split(bucket_name)[-1].replace('/','',1)
        tos_path = os.path.normpath(tos_path)+"/"
        bucket_name=one_col[0].text_input('bucket_name',bucket_name).lstrip("/").rstrip('/')
        #elif
        percentage = int(one_col[2].text_input('随机百分比显示', 100))
        one_col[3].title("")
        update_button = one_col[3].button("更新数据")
        if "data" not in st.session_state:
            st.session_state.data=''
        if update_button:
            # 其他控件有变化，重新获取数据
            data = get_all_audio(bucket_name,tos_path)
            st.session_state.all_data = sorted(data)
            st.session_state.data = sorted(data)
            st.session_state.percentage=percentage
        if st.session_state.data and st.session_state.percentage!=percentage:
            sample_size = int(len(st.session_state.all_data) * percentage * 0.01)
            #print(sample_size)
            random_sample = sorted(random.sample(st.session_state.all_data, sample_size))
            st.session_state.data = sorted(random_sample)
            st.session_state.percentage=percentage
        if st.session_state.data and "all_data" in st.session_state:
            err_data={}
            if st.session_state.percentage==100:
                df['audio'] = st.session_state.all_data
            else:
                df['audio'] = st.session_state.data
            df.index+=1
            three_col[0].dataframe(df,use_container_width=True)
            clicked_row = two_col[0].number_input(f'请输入行号以获取音乐链接:可播放音频总数{len(st.session_state.data)}', min_value=1, max_value=len(st.session_state.data), step=1)
            if 1 <= clicked_row <= len(df):
                options = ['背景音', '多人音色', '模仿音', '杂音']
                selected_audio_link = df.at[clicked_row, 'audio']
                binary_audio_data=read_file(bucket_name,selected_audio_link)
                two_col[1].text('')
                two_col[1].audio(binary_audio_data, format="audio/mp3")
        one_col[4].title("")
        dur_button = one_col[4].button("统计时长")
        if dur_button and tos_path:
            count=0
            valid_count=0
            total_dur=0
            all_task=[]
            data = get_all_audio(bucket_name,tos_path)
            #print(data)
            # for i in data:
            #     binary_audio_data=read_file(bucket_name,i)
            #     all_task.append(binary_audio_data)
            batch_size=2000
            with concurrent.futures.ThreadPoolExecutor(max_workers=15) as executor:
                for i in tqdm(range(0, len(data), batch_size), desc=f"Processing {len(data)} audio files"):
                    batch = data[i:i + batch_size]
                    batch = audio_files_generator(batch)
                    processed_audio_data = []
                    for b in batch:
                        processed_audio_data.append(b)

                    task_batch = audio_files_generator(processed_audio_data)
                    duration_futures = [executor.submit(audio_dur_from_binary, bucket_name,task)  for task in tqdm(task_batch,desc='时长统计中。。。。。')]
                    for future in concurrent.futures.as_completed(duration_futures):
                        try:
                            dur = future.result()
                            count += 1
                            if dur:
                                total_dur += dur
                                valid_count += 1
                        except Exception as e:
                            print(f"Error calculating duration: {e}")
                print(f"时长：{round(total_dur/60,3)}min")
                print(f"时长：{round(total_dur/3600,3)}h")
                print(f"总数：{str(count)}")
                print(f"有效：{valid_count}")
                three_col[0].title(f"时长：{round(total_dur/60,3)}min")
                three_col[0].title(f"时长：{round(total_dur/3600,3)}h")
                three_col[0].title(f"总数：{str(count)}")
                three_col[0].title(f"有效：{valid_count}")
                    
        #         select_tag=two_col[1].multiselect('Select Options', options)
    client.close()
        
        #         if select_tag:
        #             print(select_tag)
        #             df_err[selected_audio_link]=",".join(select_tag)
        #             print(df_err)
        # if not df_err.empty:
        #     three_col[0].dataframe(df_err)
            

            

            
        
    # for audio in audio_list:
    #     read_file(audio)
    # client.close()