import refile
import streamlit as st
import pandas as pd
import subprocess
import configparser
import os
import rclone
import requests
import json
# 执行代理配置命令

BASE_CMD_SYNC='rclone sync -P --transfer={} {}:{} {}:{}'
BASE_CMD_LSD='rclone lsd  {}:'
arguments_list=['sync','copy']
tos_dir_list=['/data/a10/lyw/dir_for_audio_slice/lq/',
            '/data/a10/melody/tts/lq/']
basemind_oss=[]
brainpp_oss=[]
def get_config_oss():
    config_path='/home/titanliang/.config/rclone/rclone.conf'
    with open(config_path,'r',encoding='utf-8') as f:
        data=f.read()
    config = configparser.ConfigParser()
    config.read_string(data)
    return config.sections()[1:]

def run_cmd(cmd):
    print(cmd)
    result =subprocess.run(cmd,shell=True, capture_output=True, text=True)
    return result.returncode, result.stdout.strip().splitlines()

if '__main__' ==__name__:
    stdout=''
    st.set_page_config(layout="wide",initial_sidebar_state ='collapsed')
    with open("env/proxy_config.txt", "r") as file:
        data = file.read()
    if data:
        proxy_config=json.loads(data)
        os.environ['all_proxy'] = proxy_config['all_proxy']
        os.environ['no_proxy'] = proxy_config['no_proxy']
        os.environ['http_proxy'] = proxy_config['http_proxy']
        os.environ['https_proxy'] = proxy_config['https_proxy']
    one_col = st.columns([4,4, 1,1])
    config_oss = get_config_oss()
    #print(config_oss)
    with one_col[0],one_col[1],one_col[2],one_col[3]:
        src_bucket = one_col[0].selectbox('SRC', config_oss.copy())
        tos_bucket=BASE_CMD_LSD.format(src_bucket)
        run_cmd(tos_bucket)
        src_path=one_col[0].text_input('SRC 路径','')
        config_oss.remove(src_bucket)
        dst_bucket = one_col[1].selectbox('DST', config_oss)
        dst_path=one_col[1].text_input('DST 路径','')
        argument=one_col[2].selectbox("参数",arguments_list)
        transfer_num=int(one_col[3].text_input("Transfer",16))
        one_col[2].subheader('')
        run_button=one_col[2].button('RUN',use_container_width =True)
        BASE_CMD_SYNC='rclone {} -P --transfer={} :{}  {}:{}'
        cmd=BASE_CMD_SYNC.format(argument,src_bucket,src_path,dst_bucket,dst_path)
        if src_path and  dst_path and run_button:
            returncode, stdout=run_cmd(cmd)
            
    if stdout:
        st.text(stdout)

    
    
    
