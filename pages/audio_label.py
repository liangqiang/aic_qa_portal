import refile
import pandas as pd
import streamlit as st
import re
from st_aggrid import AgGrid, GridOptionsBuilder
from random import randint
import time
from urllib.parse import quote ,unquote
import os
from io import BytesIO
from utils.general import render_ag_grid,download_file,get_dataframe_tar_from_oss,get_unquote
from pydub import AudioSegment
import base64
temp_dir =r'/data'
def read_txt(path):
    if path!='NA':
        with refile.smart_open(path,'r') as f:
            data=f.read()
        return data
    else:
        return 'NA'
def get_last_number_from_url(url):
    match = re.search(r'\d+', url.rsplit('_', 1)[-1])
    if match:
        return int(match.group())
    else:
        return 0
@st.cache_resource
def get_dataframe_folder(oss_path,audio_flag):
    wav_dict={}
    audio_list,txt_list=[],[]
    if '.tar.gz' in oss_path:
        wav_dict,txt_list=get_dataframe_tar_from_oss(oss_path,audio_flag)
        dict_data={'wav':list(wav_dict.keys()),'txt':txt_list}
        df=pd.DataFrame(dict_data)
    else:
        if not oss_path.endswith('/'):
            oss_path=oss_path+'/'
        for root,dir,files in refile.smart_walk(oss_path):
            for index, file in enumerate(files,start=1):
                if file.endswith('.mp3') or file.endswith('.wav') or file.endswith('.m4a'):
                    audio_file=refile.smart_path_join(root,file)
                    txt_file=audio_file.replace('.'+file.rsplit('.', 1)[-1],'.txt')
                    audio_list.append(audio_file)
                    
                    if audio_flag:
                        txt_list.append(txt_file)
                    else:
                        txt_list.append('NA')
        audio_list=sorted(audio_list)
        txt_list=sorted(txt_list)
        dict_data={'wav':audio_list,'txt':txt_list}
        df=pd.DataFrame(dict_data)
        df['wav']=df['wav'].apply(lambda x:  x.replace("s3://", 'https://oss.iap.platform.basemind.com/'))
        df['txt']=df['txt'].apply(read_txt)
        df['sort_order'] = df['wav'].apply(get_last_number_from_url)
        #print(df['sort_order'] )
        df = df.sort_values(by='sort_order')
        df = df.drop(columns=['sort_order'])
        #df=df.sort_values('wav')
    
    return df,wav_dict


if '__main__' ==__name__:
    st.set_page_config(layout="wide",initial_sidebar_state ='collapsed')
    if not os.path.exists('data/output/play_audio/'):
        os.makedirs('data/output/play_audio/')
    one_col=st.columns([6,1,1])

    with one_col[0],one_col[1],one_col[2]:
            #global df,file_name
        oss_path=one_col[0].text_input('OSS_PATH:','https://oss.iap.platform.basemind.com/libingx-data/lq/test/guoyu/1/').strip()
        audio_flag=['Audio and Txt','Only Audio']
        audio_flag=one_col[1].radio("选择类型：",audio_flag)
        if audio_flag=="Audio and Txt":
            audio_flag=True
        else:
            audio_flag=False
        if oss_path.endswith('/'):
            file_name=oss_path.split('/')[-2]
        else: 
            file_name=oss_path.split('/')[-1].replace('.tar.gz', "")
        file_name=get_unquote(file_name)
        oss_path=oss_path.replace('https://oss.iap.platform.basemind.com/','s3://')
        oss_path=get_unquote(oss_path)
        percentage=int(one_col[2].text_input('随机显示百分比的数据',100))
        
        if 'percentage' not in st.session_state or percentage==100:
            df,wav_dict=get_dataframe_folder(oss_path,audio_flag)
            st.session_state.percentage = 100
            st.session_state.num=len(df.copy())
            st.session_state.df=df.copy()
            st.session_state.df_all=df.copy()
            df.reset_index(drop=True, inplace=True)
            one_col[0].write(f"oss 地址：{oss_path}。 音频总数：{st.session_state.num}")
            df.index+=1
            st.session_state.df=df.copy()
            st.session_state.wav_dict=wav_dict
        if st.session_state.percentage != percentage :
            display_rows = int(percentage/100 * st.session_state.num)
            df_ = st.session_state.df_all.sample(display_rows)
            df_.reset_index(drop=True, inplace=True)
            #if 'df' not in st.session_state:
            df_.index+=1
            st.session_state.df = df_.copy()
            st.session_state.percentage = percentage
            #df=st.session_state['df_']
    df_data=st.session_state.df
    st.dataframe(df_data,use_container_width=True)
    #download_xlsx()
    
    
    two_col=st.columns([2,2,1,1])
    three_col=st.columns([5,1,1])
    #df_save=pd.DataFrame(columns=['wav','txt'])
    save_file=f'data/output/play_audio/{file_name}_change.csv'
    if not os.path.exists(save_file):
        df_chang=pd.DataFrame(columns=['wav','txt','tag'])
    else:
        df_chang=pd.read_csv(save_file,index_col=0)
    with two_col[0],two_col[1],two_col[2],two_col[3]:
        clicked_row = two_col[0].number_input(f'请输入行号以获取音乐链接:可播放音频总数{len(df_data)}', min_value=1, max_value=len(df_data), step=1)
        # 播放音乐
        #if two_col[1].button('Play'):
        if 1 <= clicked_row <= len(df_data):
            selected_audio_url = df_data.at[clicked_row, 'wav']
            select_txt=df_data.at[clicked_row , 'txt']
            two_col[1].text('')
            autoplay_flag=two_col[3].checkbox('自动播放')
            #print(selected_audio_url)
            if '.tar.gz' in oss_path :
                binary_audio_data = st.session_state.wav_dict[selected_audio_url]
                two_col[1].audio(binary_audio_data, format="audio/mp3")
            elif os.path.isdir(oss_path):
                #binary_audio_data = st.session_state.wav_dict[selected_audio_url]
                with open(selected_audio_url,'rb') as file:
                    two_col[1].audio(file, format="audio/mp3")

            else:
                if autoplay_flag:
                    two_col[1].markdown(f'<audio src="{selected_audio_url}" controls autoplay></audio>', unsafe_allow_html=True)
                else:
                    two_col[1].markdown(f'<audio src="{selected_audio_url}" controls></audio>', unsafe_allow_html=True)
            two_col_=two_col[2].radio("选择一个选项", ["修改", "删除","空白音",'语气词','其他'])
            if two_col_=='空白音':
                three_col[0].markdown(selected_audio_url)
                txt_change=three_col[0].text_area(select_txt,select_txt.replace('\n', '')+'00:00:00-00:00:00')
            else:
                three_col[0].markdown(selected_audio_url)
                txt_change=three_col[0].text_area(select_txt,select_txt.replace('\n', ''))
                # 创建一个保存按钮
            three_col[1].text(' ')
            save_button = three_col[1].button("添加")
            rm_button= two_col[3].button("清空")
            if save_button:
                if selected_audio_url in df_chang['wav'].tolist():
                    three_col[0].error('重复')
                    st.stop()
                if two_col_=='删除':
                    tag='删除'
                elif two_col_=='修改':
                    tag='需修改'
                elif two_col_=='空白音':
                    tag='空白音'
                elif two_col_=='语气词':
                    tag='语气词'
                elif two_col_=='其他':
                    tag='其他'
                #df_chang = df_chang.reset_index(drop=True)
                #df_chang = df_chang.reset_index(drop=True)
                txt_change=txt_change
                new_data = pd.DataFrame([{'wav':selected_audio_url,'txt':txt_change,'tag':tag}])

                df_chang = pd.concat([df_chang, new_data],ignore_index=True)
                #df_chang=df_chang.drop('index')
            if rm_button:
                df_chang = df_chang.drop(df_chang.index)
    df_chang.to_csv(save_file)
    df_chang,go=render_ag_grid(df_chang)

    # 显示Ag-Grid表格
    grid_response = AgGrid(
        df_chang,
        gridOptions=go,
        theme='streamlit',
        fit_columns_on_grid_load=True,
        enable_enterprise_modules=True
        # height=300,
        # width='100%',
    )
    selected_rows=grid_response['selected_rows']
    if selected_rows:
        delete_row_button = three_col[1].button("删除行")
        if delete_row_button:
            selected_indices = [row['index'] for row in selected_rows]
            df_chang=df_chang.drop(selected_indices)
            df_chang.to_csv(save_file,index=False)
            st.experimental_rerun()
        else:
            selected_rows = pd.DataFrame(grid_response['selected_rows']).drop(['_selectedRowNodeInfo','index'], axis=1)
            #st.dataframe(df_select_row)
            df_chang=selected_rows
    
    if  os.path.exists(save_file):

        #st.dataframe(df_chang,use_container_width=True)
        href=download_file(save_file)
        three_col[2].text('DownLoad')
        three_col[2].markdown(href,unsafe_allow_html=True)
