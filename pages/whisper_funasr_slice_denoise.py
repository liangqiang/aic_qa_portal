import tempfile
import requests
import sys
import os
import streamlit as st
from datetime import datetime
import concurrent.futures
import string
import time
import shlex
import json
import threading
from utils.general import audio_dur_from_binary
import shutil
from urllib.parse import quote
temp_dir =r'/data'
# 清除代理设置
os.environ['all_proxy'] = ''
os.environ['no_proxy'] = ''
os.environ['http_proxy'] = ''
os.environ['https_proxy'] = ''
now = datetime.now().strftime("%Y%m%d")
url='http://liwen.arvinli.basemind-core.svc.platform.basemind.local:23332/asr'
params_lock = threading.Lock()
def base_request(params_args,audio_file,root_path):
    
    ###['{"result":true,"details":"Success for funasr"}', '{"result":true,"details":"Success for faster whisper"}', '{"result":true,"details":"Success for whisper"}']
    params = {
        'root_path': root_path, # 切片服务输出目录，只支持 /data/a10/ 开头的路径
        'separate_vocals': True, # 是否开启人声分离
        'remove_silence': params_args['remove_silence'], # 是否切除空白音
        'whisper_transcribe': params_args['whisper_transcribe'], # 是否开启 whisper 服务
        'whisper_slice': params_args['whisper_slice'], # 是否根据 whisper 结果做切片服务
        'whisper_annos': params_args['whisper_annos'], # 是否输出 whisper 对应的annos.json
        'faster_whisper_transcribe': False, # 是否开启 faster whisper 服务
        'faster_whisper_slice': False, # 是否根据 faster whisper 结果做切片服务
        'faster_whisper_annos': False,# 是否输出 faster whisper 对应的annos.json
        'funasr_transcribe': params_args['funasr_transcribe'], # 是否开启 funasr 服务
        'funasr_slice': params_args['funasr_slice'], # 是否根据 funasr 结果做切片服务
        'funasr_annos': params_args['funasr_annos'] # 是否输出 funasr 对应的annos.json
    }
    res=""
    result_bool_list=[]
    params_copy=params.copy()
    for index, _ in enumerate(range(2),start=1):
        try:
            type_bool,label_list={},[]
            with open(audio_file, "rb") as file:
                if audio_file=='【声音恋人】腹黑奶狗弟弟又在对姐姐撒娇，原来因为这个？（年下_6106853.m4a':
                    print(params_copy)
                print(f'START:{audio_file}')
                #print(f"文件长度：{len(file)}")
                res = requests.post(url, params=params_copy, files={"file": file})
                result_bool_list = [json.loads(item)["result"] for item in res.json()]
                if res.status_code!=200:
                    print(f"ERROR_请求失败 retry{index}次: {audio_file}{res.status_code}")
                    continue
                elif res.status_code==200 and all(result_bool_list):
                    print(f"SUCESS: {audio_file}")
                    break  # 如果请求成功，则跳出循环
                elif res.status_code==200:
                    current_thread_id = threading.current_thread().ident
                    print(f"请求成功返回结果Fail-{current_thread_id}_{audio_file}:",res.json())
                    #type_bool={'funasr_slice':False,"whisper_slice":False, "faster_whisper_slice":False}
                    type_bool={'funasr_slice':False,"whisper_slice":False}
                    result_type_list = [json.loads(item)["details"] for item in res.json()]
                    for result_bool,result_type in zip(result_bool_list,result_type_list):
                        if not result_bool :
                            if  "run_funasr" in result_type:
                                type_bool['funasr_slice']=True
                            elif  "funasr_pipeline" in result_type:
                                type_bool['funasr_slice']=True
                        elif not result_bool :
                            if "run_whisper" in result_type:
                                type_bool['whisper_slice']=True
                            elif "whisper_pipeline" in result_type:
                                type_bool['whisper_slice']=True
                        # elif not result_bool and "faster_whisper_mode" in result_type:
                        #     type_bool['faster_whisper_slice']=True
                    with params_lock:
                        for k,v in type_bool.items():
                            if v:
                                label_list.append(k)
                            params_copy[k]=v
                                
                    
                    print(f"{current_thread_id}-{type_bool}")
                    print(f"新参数配置-{current_thread_id}-{params_copy}")
                    print(f'{"&".join(label_list)}--请求成功无返回结果retry {index}次：{audio_file}----{res.json()}')
        except Exception as e:
            print(f"ERROR_请求异常 retry{index}次: {audio_file}-{e}")
    return res,audio_file,root_path,result_bool_list,type_bool

def write_data(basename,txt):
    output_path=r'data/output'
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    save_path=os.path.join(output_path,basename)
    with open(f"{save_path}.txt",'a',encoding='utf-8') as w:
        w.write(txt)
def send_audio_in_batch(path):
    num=1
    total_dur=0
    max_dur=18000
    audio_dict={}
    for root,dirs,files in os.walk(path):
        for file in files:
            if file.endswith(('.wav','.mp3','.m4a')):
                audio_file=os.path.join(root,file)
                duration_seconds,_=audio_dur_from_binary(audio_file)
                total_dur+=duration_seconds
                audio_dict[audio_file]=file
            if total_dur>max_dur:
                save_path=path+"_"+str(num)
                if not os.path.exists(save_path):
                    os.makedirs(save_path)
                num+=1
                for k,v in audio_dict.items:
                    shutil.move(k,os.path.join(save_path,v))
                audio_dict.clear()
                total_dur=0
def save_file(path,data,data_name):
    #address='data/log/'
    with open(os.path.join(path,data_name), 'wb+') as f:
        f.write(data)
    return path
if '__main__' ==__name__:
    st.set_page_config(layout="wide",initial_sidebar_state ='collapsed')
    BASE_PATH = f'/data/a10/lq/{now}' # 只支持 /data/a10/ 开头的路径
    all_task=[]
    one_col=st.columns([1,6,1,1,1,1,1,1,1])
    two=st.columns([1,1,1,1,1,1])
    sucess_list,fail_list,err_list=[],[],[]
    params_args={
        'whisper_transcribe':True,
        'whisper_slice':True,
        'whisper_annos':True,
        'funasr_slice':True,
        'funasr_annos':True,
        'funasr_transcribe':True,
        'remove_silence':True,
        #'faster_whisper_slice':True,
        }
    save_path=r'/data/tmp/upload'
    os.makedirs(save_path,exist_ok=True)
    #root_path=os.path.join(BASE_PATH,audio_path.split('/')[-1].split('.')[0])
    with one_col[0],one_col[1],one_col[2],one_col[3],one_col[4],one_col[5],one_col[6],one_col[7],one_col[8]:
        count=0
        FLAG_LIST=['True','False']
        input_type_list=['ws2','local']
        input_type=one_col[0].radio('input type',input_type_list)
        if input_type=='local':
            uploaded_files =one_col[1].file_uploader("UP LOAD",type=[".mp3", ".wav", ".m4a"], accept_multiple_files=True)
            if uploaded_files  is not None :
                for uploaded_file in uploaded_files:
                    save_file(save_path,uploaded_file.getvalue(),uploaded_file.name)
                print(save_path)
                input_path=save_path
        else:
            input_path=one_col[1].text_input("audio path","/data/tmp/")
        max_work=int(one_col[2].text_input("work",10))
        whisper_slice_flag=one_col[3].selectbox("whisper slice",FLAG_LIST)
        funasr_slice_flag=one_col[4].selectbox("funasr slice",FLAG_LIST)
        #faster_whisper_slice_flag=one_col[4].selectbox("faster&whisper",FLAG_LIST[::-1])
        whisper_annos_flag=one_col[5].selectbox("whisper_annos",FLAG_LIST)
        funasr_annos_flag=one_col[6].selectbox("funasr_annos",FLAG_LIST)
        remove_silence_flag=one_col[7].selectbox("remove silence",FLAG_LIST)
        one_col[8].subheader('')
        run_button=one_col[8].button("Start",use_container_width =True)
        params_args['whisper_slice']=whisper_slice_flag
        params_args['funasr_slice']=funasr_slice_flag
        #params_args['faster_whisper_slice']=faster_whisper_slice_flag
        params_args['remove_silence']=remove_silence_flag
        params_args['whisper_annos']=whisper_annos_flag
        params_args['funasr_annos']=funasr_annos_flag
        if whisper_slice_flag is False and whisper_annos_flag is False:
            params_args['whisper_transcribe']=False
        if funasr_slice_flag is False and funasr_annos_flag is False:
            params_args['funasr_transcribe']=False
        if os.path.isdir(input_path) and run_button:
            basename=os.path.basename(input_path)
            BASE_PATH=os.path.join(BASE_PATH,basename)
            for root,dirs,files in os.walk(input_path):
                for file in files:
                    if file.endswith(('.wav','.mp3','.m4a')):
                        pre,suff=os.path.splitext(file)
                        #pre=remove_punctuation_whitespace_special_characters(pre)
                        root_path=os.path.join(BASE_PATH,pre)
                        file_path=os.path.join(root,file)
                        all_task.append((file_path,root_path))
                        count+=1
        elif os.path.isfile(input_path) and run_button:
            count=1
            basename=os.path.basename(input_path)
            #basename=remove_punctuation_whitespace_special_characters(basename)
            pre,suff=os.path.splitext(basename)
            BASE_PATH=os.path.join(BASE_PATH,pre)
            file_path=input_path
            all_task.append((file_path,BASE_PATH))
        res_count=0
        #ThreadPoolExecutor  ProcessPoolExecutor
        with concurrent.futures.ThreadPoolExecutor(max_workers=max_work) as executor:
            futures=[executor.submit(base_request,params_args,task[0],task[1]) for task in all_task]
            executor.shutdown(wait=True)
        for future in concurrent.futures.as_completed(futures):
            res,file,root_path,result_bool_list,type_bool=future.result()
            if res.status_code!=200:
                err_list.append(file)
            elif res.status_code==200 and all(result_bool_list):
                sucess_list.append({file:f"火山云：{root_path}"})
                res_count+=1
            elif not all(result_bool_list):
                #fail_list.append((type_bool,file,root_path,type_bool))
                return_check=[k for k,v in type_bool.items() if not v]
                fail_list.append("&".join(return_check)+"--"+file)
                
        # if fail_list:
        #     print('---------------------------------------处理FAIL的任务---------------------------------------------')
        #     time.sleep(5)
        #     with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
        #         futures=[executor.submit(base_request,task[0],task[1],task[2]) for task in fail_list]
        #         executor.shutdown(wait=True)
        #     fail_list=[]
        #     for future in concurrent.futures.as_completed(futures):
        #         res,file,root_path,result_bool_list,type_bool=future.result()
        #         if  res.status_code!=200:
        #             err_list.append(file)
        #         elif res.status_code==200 and all(result_bool_list):
        #             sucess_list.append({file:f"火山云：{root_path}"})
        #         elif not all(result_bool_list):
        #             return_check=[k for k,v in type_bool.items() if not v]
        #             fail_list.append("&".join(return_check)+"--"+file)


    if sucess_list:
        print(f"python3 /data/a10/lq/slice_audio_process.py --slice --local {BASE_PATH}")
            #"python3 /data/a10/lq/slice_audio_process.py--annos --local {BASE_PATH} ")
        sucess_list=[f"SUCESS:{i}" for i in sucess_list]
        write_data(basename,"\n".join(sucess_list))
        if os.listdir(save_path):
            shutil.rmtree(save_path)
        #print(sucess_list)
    if fail_list:
        fail_list=[f"FAIL_请求成功无返回结果:{i}" for i in fail_list]
        write_data(basename,"\n".join(fail_list))
        print(fail_list)
    if err_list:
        err_list=[f"ERROR_请求失败:{i}" for i in err_list]
        write_data(basename,"\n".join(err_list))
        print(err_list)
    if sucess_list:

        st.write(f" python3 /data/a10/lq/slice_audio_process.py --slice --local {BASE_PATH}")

        st.text("\n".join(sucess_list))
        #print(sucess_list)
    if fail_list:
        st.text("\n".join(fail_list))
    if err_list:
        st.text("\n".join(err_list))

    #print(f" python3 /data/a10/lq/slice_audio_process.py --loacl {BASE_PATH}")
    #base_request(audio_path,root_path)

