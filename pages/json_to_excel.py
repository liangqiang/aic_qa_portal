import refile
from st_aggrid import AgGrid, GridOptionsBuilder
import streamlit as st
import json
import pandas as pd
from datetime import datetime
import tempfile
from utils.general import render_ag_grid,download_file
now = datetime.now().strftime("%Y%m%d")
json_path=''
#path=r's3://libingx-data/oasis/bmk-eval-output/20231027/compare-bmk-11_NVWA-v7.1-温语-results_20231027172954.json'
@st.cache_resource
def json_to_excel(path):
    with refile.smart_open(path,'r',encoding='utf-8') as f:
        data=f.read()
        data=json.loads(data)
    df=pd.DataFrame(data)
    num=len(data)
    #df['Index'] = [i for i in range(0,len(data))]
    #AgGrid(df,index=False)
    return df,num

def get_file_list(path_json):
    file_list=refile.smart_listdir(path_json)
    file_list=[i for i in file_list if i.endswith('.json')]
    file_list=sorted(file_list,reverse=True)
    return file_list

if '__main__' ==__name__:
    row_height =35
    table_list=["Excel",'Dataframe','Table']
    ROOT_PATH=r's3://libingx-data/oasis/bmk-eval-output/'
    st.set_page_config(layout="wide",initial_sidebar_state ='collapsed')
    st_one=st.columns([1,3,2,1])
    st_two=st.columns([1,3,2,1])
    folder_list=refile.smart_listdir(ROOT_PATH)
    folder_list=[i for i in folder_list if refile.smart_isdir(refile.smart_path_join(ROOT_PATH,i))]
    #st.text(folder_list)
    folder_list=sorted(folder_list,reverse=True)
    one_col=st.tabs( table_list)
    with st_one[0],st_one[1],st_one[2],st_two[0],st_two[1],st_two[2]:
        folder=st_one[0].selectbox('Input',folder_list)
        folder2=st_two[0].selectbox('Compare',folder_list)
        path_json=refile.smart_path_join(ROOT_PATH,folder)
        path_json2=refile.smart_path_join(ROOT_PATH,folder2)
        file_list=get_file_list(path_json)
        file_list2=get_file_list(path_json2)
        file_json_one=st_one[1].selectbox('Input',file_list)
        file_name=file_json_one.replace('.json', '')
        #file_list_two=[i for i in file_list2 if i.startswith(file_name)]
        file_json_two=st_two[1].selectbox('Compare',file_list2)
        
        df,num=json_to_excel(refile.smart_path_join(path_json,file_json_one))
        df2,num2=json_to_excel(refile.smart_path_join(path_json2,file_json_two))
        #if file_json_one.split('result')
        #df.to_excel(f'{now}_{file_name}.xlsx')
        #href=download_file(f'{now}_{file_name}.xlsx')
        #st_two[2].markdown(href,unsafe_allow_html=True)
        st_one[3].markdown(f'总数：{str(num)}')
        #st_two[3].markdown(f'总数：{str(num)}')
        df=df.drop(columns=['session_id'])
        df2=df2.drop(columns=['session_id'])
        column_names = df.columns[1:]
        selected_options = st_one[2].multiselect('选择多个选项:', column_names,default="response")
        merged_df = pd.merge(df, df2, on='prompt', how='inner', suffixes=('_Input', '_Compare'))
        # print(selected_options)
        # print(type(selected_options))
        show_colums=[]
        if selected_options:
            
        #selected_options=['prompt']
            for x in selected_options:
                show_colums.append(x+'_Input')
                show_colums.append(x+'_Compare')

        else:
            for x in column_names:
                show_colums.append(x+'_Input')
                show_colums.append(x+'_Compare')
        #print(selected_options)
        show_colums.insert(0,'prompt')
        height=num*row_height
        merged_df=merged_df[show_colums]
    with one_col[0]:
        #grid_options = GridOptionsBuilder.from_dataframe(merged_df[show_colums]).build()
        #grid_options['defaultColDef']['wrapText'] = True
        merged_df,go=render_ag_grid(merged_df)

        # 显示Ag-Grid表格
        grid_response = AgGrid(
            merged_df,
            gridOptions=go,
            theme='streamlit',
            fit_columns_on_grid_load=True,
            enable_enterprise_modules=True
            # height=300,
            # width='100%',
        )
        if len(grid_response['selected_rows'])>0:
            df_select_row = pd.DataFrame(grid_response['selected_rows']).drop(['_selectedRowNodeInfo','index'], axis=1)
            #st.dataframe(df_select_row)
            merged_df=df_select_row
    with one_col[1]:
        one_col[1].dataframe(merged_df,use_container_width=True, height=height)
    with one_col[2]:
        one_col[2].table(merged_df)
    merged_df.to_excel(f'data/output/{now}_{file_name}.xlsx')
    href=download_file(f'data/output/{now}_{file_name}.xlsx')
    st_two[2].text('DownLoad')
    st_two[2].markdown(href,unsafe_allow_html=True)