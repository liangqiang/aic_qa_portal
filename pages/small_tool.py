from utils.general import audio_dur_from_binary,audio_files_generator
from utils import general
import refile
import streamlit as st
import concurrent.futures
import os
import pandas as pd
from pydub import AudioSegment
from datetime import datetime
import shutil
from datetime import datetime
import urllib.parse
from urllib.parse import urlparse
import tempfile
from typing import Dict
from tqdm import tqdm
import gc
import psutil
import time
import dill 
now = datetime.now().strftime("%Y%m%d")
wav_dict={}
temp_dir =r'/data/tmp'
def classify_path(path):
    parsed_url = urlparse(path)
    if parsed_url.scheme == 's3':
        return 'S3 Path'
    else:
        return 'Local Path'


def process_audio_file(file: str):
    wav_dict[file] = file
    return wav_dict

def process_local_path(oss_path: str):
    print('Loacl')
    all_task=[]
    for root, dirs, files in os.walk(oss_path):
        for file in tqdm(files,desc="task 添加。。。。"):
            if file.endswith(('.wav', '.mp3', '.m4a')):
                file=os.path.join(root,file)
                all_task.append(file)
    return all_task
def process_oss_path(oss_path: str):
    print('OSS')
    all_task=[]
    for root, dirs, files in refile.smart_walk(oss_path):
        for file in tqdm(files,desc="task 添加。。。。"):
            if file.endswith(('.wav', '.mp3', '.m4a')):
                file=refile.smart_path_join(root,file)
                all_task.append(file)
    return all_task


if '__main__' ==__name__:
    st.set_page_config(layout="wide",initial_sidebar_state ='collapsed')
    if not os.path.exists('/data/tmp/audio_tmp'):
        os.makedirs('/data/tmp/audio_tmp')
    one_col=st.columns([8,1])
    all_task=[]
    err_audio_list=[]
    total_duration = 0  # 初始化总时长
    count = 0  # 初始化音频总数
    error_count=0 
    #two_col=st.columns([2,1])
    with one_col[0],one_col[1]:
        oss_path=one_col[0].text_input('获取音频时长 OSS/本地 地址',"s3://denjiliang/lq1106/20231103_10分40秒_guoyu_gpt-男/").strip().replace('https://oss.iap.platform.basemind.com/', 's3://')
        pairs_path=one_col[0].text_input('本地音频 地址(删除本地0秒音频/空文本）',"").strip().replace('https://oss.iap.platform.basemind.com/', 's3://')
        cut_audio_path=one_col[0].text_input('本地待切割音频路径',"").strip()
        # 设置整体按钮的宽度
        #one_col[1].set_option('deprecation.showfileUploaderEncoding', False)  # 用于隐藏按钮的文本
        one_col[1].subheader('')
        audio_button=one_col[1].button('获取音频时长',use_container_width =True)
        one_col[1].subheader('')
        rm_button=one_col[1].button('删除本地0秒音频/空文本',use_container_width =True)
        one_col[1].subheader('')
        cut_button=one_col[1].button('切割空白音',use_container_width =True)
        if not os.path.exists('data/output/small_tool/'):
            os.makedirs('data/output/small_tool/')
        
        ### 获取oss音频总时长
        if audio_button:
            temp_directory = tempfile.mkdtemp(dir=temp_dir)
            # URL解码
            oss_path = urllib.parse.unquote(oss_path)
            audio_type=classify_path(oss_path)
            print(audio_type)
            if oss_path.endswith(('.mp3','.wav','.m4a')):
                audio_type='S3 Path'
                wav_dict=process_audio_file(oss_path)
                print(wav_dict)
                #audio_type = 'S3 Path'
            elif oss_path.endswith('.tar.gz'):
                wav_dict, txt_list = general.get_dataframe_tar_from_oss(oss_path,False)
                audio_type = 'S3 Path'
            elif audio_type == "Local Path":
                all_task_dur=process_local_path(oss_path)
            elif audio_type =="S3 Path":
                tool_size = refile.smart_getsize(oss_path) / (1024 ** 3)
                print(tool_size)
                all_task_dur=process_oss_path(oss_path)
            if not wav_dict:
                with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor_dur:
                    futures_dur=[executor_dur.submit(process_audio_file,task) for task in all_task_dur]
                    executor_dur.shutdown(wait=True)
            all_task=[(k,v) for k,v in wav_dict.items()]
            error_task=[]
            batch_size = 2000  # 根据系统资源和任务需求调整批次大小
            for i in tqdm(range(0, len(all_task), batch_size),desc=f"all audio files: {len(all_task)}条"):
                #st.experimental.clear_cache
                batch = all_task[i:i + batch_size]
                audio_generator = audio_files_generator(batch)
                with concurrent.futures.ThreadPoolExecutor(max_workers=15) as executor:
                    futures = [executor.submit(audio_dur_from_binary, task[0], task[1],audio_type) for task in audio_generator]
                    #concurrent.futures.wait(futures)
                    for index, future  in  tqdm(enumerate(concurrent.futures.as_completed(futures))):
                        try:
                            duration, error_audio = future.result()
                            count += 1
                            if error_audio:
                                error_count += 1
                                err_audio_list.append(error_audio)
                            total_duration += duration
                        except Exception as e:
                            
                            task=batch[index]
                            print(f"{index}:",e)
                            error_task.append(task)

            else:
                if error_task:
                    print('============================================处理报错任务============================================================')
                    error_audio_generator = audio_files_generator(error_task)
                    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
                        error_futures = [executor.submit(audio_dur_from_binary, task[0], task[1], audio_type) for task in tqdm(error_audio_generator, desc=f"ERROR-Processing audio files: {len(error_task)}条")]
                        #concurrent.futures.wait(error_futures)
                    for  error_future  in  tqdm(concurrent.futures.as_completed(error_futures)):
                        duration, error_audio = error_future.result()
                        count += 1
                        if error_audio:
                            error_count += 1
                            err_audio_list.append(error_audio)
                        total_duration += duration
                #print(oss_path)
                print(f"时长：{round(total_duration/60,3)}min")
                print(f"时长：{round(total_duration/3600,3)}h")
                print(f"总数：{str(count)}")
                print(f"有效：{count-error_count}")
                one_col[0].write(oss_path)
                one_col[0].title(f"时长：{round(total_duration/60,3)}min")
                one_col[0].title(f"时长：{round(total_duration/3600,3)}h")
                one_col[0].title(f"总数：{str(count)}")
                one_col[0].title(f"有效：{count-error_count}")
            if err_audio_list:
                one_col[0].text(f"Fail总数：{err_audio_list}")
                shutil.rmtree(temp_directory)

        if rm_button:
            pairs_path = urllib.parse.unquote(pairs_path)
            print(pairs_path)
            rm_audio_count=general.audio_process(pairs_path)
            one_col[0].title(f'删除音频{str(rm_audio_count)}对/个')
                
        if cut_button:
            save=fr'data/output/small_tool/cut_audio/{now}'
            if not os.path.exists(save):
                os.makedirs(save)
            df=pd.DataFrame()
            timestamp_list,cut_audio_list,txt_list,none_timestamp_list=[],[],[],[]
            for root,dir,files in os.walk(cut_audio_path):
                for file in files:
                    if file.endswith('.wav')  or file.endswith('.mp3')  or file.endswith('.m4a') :
                        wav_file=os.path.join(root,file)
                        txt_file=file.replace(file.rsplit('.')[-1], 'txt')
                        with open(os.path.join(root,txt_file),'r',encoding='utf-8') as f:
                            data=f.read()
                        if '-' in data and '00' in data:
                            timestamp=data.split('[ZH]')[-1]
                            cut_audio,timestamp_=general.cut_and_replace_audio(wav_file,timestamp,save)
                            data=data.rsplit('[ZH]',1)[0]+'[ZH]'
                            with open(os.path.join(save,txt_file),'w',encoding='utf-8') as w:
                                w.write(data)
                            #shutil.copy(os.path.join(root,txt_file),os.path.join(save,txt_file))
                            timestamp_list.append(timestamp_)
                            cut_audio_list.append(cut_audio)
                            txt_list.append(data)
                        else:
                            none_timestamp_list.append(wav_file)
                            none_timestamp_list.append(wav_file)
    
            df['wav']=cut_audio_list
            df['timestamp']=timestamp_list
            df['txt']=txt_list
            if none_timestamp_list:
                one_col[0].write(f"无时间戳的文本{none_timestamp_list}")
            one_col[0].dataframe(df,use_container_width=True)
    one_col_=one_col[0].columns([7,1])
    one_col[1].subheader('')
    one_col[1].subheader('')
    tar_button=one_col[1].button('远程压缩tar包',use_container_width =True)
    one_col[1].subheader('')

    del_button=one_col[1].button('删除后缀文件',use_container_width =True)
    with one_col[1],one_col_[0],one_col_[1]:
        oss_tar_path=one_col_[0].text_input('远程打包 OSS地址',"").strip().replace('https://oss.iap.platform.basemind.com/', 's3://')
        tar_name=str(one_col_[1].text_input('Tar name prefix',""))
        
        if tar_button:
            
            oss_tar_path = urllib.parse.unquote(oss_tar_path)
            if refile.smart_isdir(oss_tar_path):
                if not tar_name:
                    tar_name=os.path.basename(oss_tar_path)
                print(tar_name)
                total_size,tar_name=general.archive_tar_dir(oss_tar_path,tar_name.replace('.tag.gz',''))
            else:
                if not tar_name:
                    tar_name=os.path.basename(oss_tar_path)
                print(tar_name)
                total_size,tar_name=general.archive_tar(oss_tar_path,tar_name.replace('.tag.gz',''))
                oss_tar_path=oss_tar_path.rsplit('/',1)[0]
            oss_tar_path="/".join(oss_tar_path.split("/")[:-2])
            one_col[0].write(f"link:{oss_tar_path.replace('s3://', 'https://oss.iap.platform.basemind.com/')}")
            one_col[0].write(f'size:{str(total_size)},tar:{tar_name}')
        with one_col[1],one_col_[0],one_col_[1]:
            oss_path=one_col_[0].text_input('删除后缀文件 OSS地址',"").strip().replace('https://oss.iap.platform.basemind.com/', 's3://')   
            oss_path=general.get_unquote(oss_path)
            suf=one_col_[1].text_input('后缀名称：例如“.txt”',"")
            if del_button:
                del_file=[]
                if suf=='null':
                    refile.smart_remove(oss_path)
                    del_file.append(oss_path)
                elif suf and suf!='null':
                    for root,dirs ,files in refile.smart_walk(oss_path):
                        for file in files:
                            if file.endswith(suf):
                                del_file_path=refile.smart_path_join(root,file)
                                refile.smart_remove(del_file_path)
                                del_file.append(del_file_path)
                
                if del_file:
                        one_col[0].write(del_file)
                    
    two_col=st.columns([4,4,1])
    two_col[2].subheader('')
    with two_col[0],two_col[1],two_col[2]:
        src_path=two_col[0].text_input('输入地址',"").strip().replace('https://oss.iap.platform.basemind.com/', 's3://')   
        dsr_path=two_col[1].text_input('目标地址',"").strip().replace('https://oss.iap.platform.basemind.com/', 's3://')   
        rname_button=two_col[2].button('rname',use_container_width =True)
        if rname_button and src_path and dsr_path:
            src_path = urllib.parse.unquote(src_path)
            dsr_path = urllib.parse.unquote(dsr_path)
            if refile.smart_isdir(src_path) :
                refile.smart_move(src_path,dsr_path)
            elif refile.smart_isfile(src_path) :
                refile.smart_rename(src_path,dsr_path)
            two_col[0].markdown(f"{src_path} TO {dsr_path}")