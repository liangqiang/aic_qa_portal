from gradio_client import Client
import os
import shutil
import pandas as pd
import streamlit as st
from datetime import datetime
import refile
import tempfile
from utils.general import archive_tar_dir, get_unquote
import pickle
import shutil
import json




### 保存代理配置
original_env = {
    'all_proxy': os.environ.get('all_proxy', ''),
    'no_proxy': os.environ.get('no_proxy', ''),
    'http_proxy': os.environ.get('http_proxy', ''),
    'https_proxy': os.environ.get('https_proxy', ''),
}
if not os.path.exists('env/proxy_config.txt'):
    with open("env/proxy_config.txt", "w") as file:
        file.write(json.dumps(original_env))
# 清除代理设置
os.environ['all_proxy'] = ''
os.environ['no_proxy'] = ''
os.environ['http_proxy'] = ''
os.environ['https_proxy'] = ''
tmp_dir=r'/data/tmp'
# # 获取或初始化状态
# state_file_path = 'state.pkl'

# if os.path.exists(state_file_path):
#     with open(state_file_path, 'rb') as f:
#         state = pickle.load(f)
# else:
#     state = {'run_count': 0, 'file_name': '', 'file_index': 0}

st.set_page_config(layout="wide", initial_sidebar_state='collapsed')
now = datetime.now().strftime("%Y%m%d")
client = Client("http://10.144.0.75:28861/")

temp_dir = tempfile.mkdtemp(dir=tmp_dir)

def get_model_list():
    result = client.predict('na', fn_index=0)
    return result['choices']

def get_user(model):
    result = client.predict(model, fn_index=1)
    return result['choices']

def get_audio(model_and_user, txt, config_list):
    result = client.predict(
        txt,
        *model_and_user,
        "简体中文",
        *config_list,
        fn_index=2
    )
    audio_file = result[-2]
    txt_file = audio_file.replace('.wav', 'txt')

    with open(txt_file, 'w', encoding='utf-8') as w:
        w.write('[ZH]'+txt+'[zh]')
    return audio_file, txt_file, txt

def mv_audio_and_txt(dataframe, local_save, rename_flag):
    df=pd.DataFrame()
    num = len(str(len(dataframe)))
    audio_list=[]
    txt_list=[]
    for index, (audio_path, txt_path, txt) in enumerate(dataframe.itertuples(index=False, name=None), start=1):
        new_audio_path = os.path.join(local_save, f"{index:0{num}d}{'_' + txt if rename_flag == 'rname' else ''}.wav")
        new_txt_path = os.path.join(local_save, f"{index:0{num}d}{'_' + txt if rename_flag == 'rname' else ''}.txt")
        shutil.move(audio_path, new_audio_path)
        shutil.move(txt_path, new_txt_path)
        audio_list.append(new_audio_path)
        txt_list.append(txt)
    df['audio']=audio_list
    df['txt']=txt_list
    return df
    
        
if __name__ == "__main__":
    model_list = get_model_list()
    dict_model_user = {model: get_user(model) for model in model_list}
    one_col = st.columns([3, 3, 1, 1, 1, 1, 1, 1])
    file_name_tag = ['random', 'rname']
    df = pd.DataFrame()
    df_process=pd.DataFrame()
    with one_col[0], one_col[1], one_col[2], one_col[3], one_col[4], one_col[5], one_col[6], one_col[7]:
        model = one_col[0].selectbox("模型", model_list)
        user = one_col[1].selectbox("音色", dict_model_user[model])
        speed = float(one_col[2].text_input("语速", 1))
        mixing_ratio = float(one_col[3].text_input('混合比', 0.2))
        emotion = float(one_col[4].text_input('感情', 0.6))
        phoneme = float(one_col[5].text_input('音素长度', 0.8))
        config_list = [speed, mixing_ratio, emotion, phoneme]
        txt_type = one_col[6].selectbox('类型', ['OSS', "UP"])
        rename_flag = one_col[7].radio("文件命名", file_name_tag)
    two_col = st.columns([11, 1])
    with two_col[0], two_col[1]:
        if txt_type == 'OSS':
            oss_path = two_col[0].text_input('OSS 地址：', "").strip()
            oss_path = get_unquote(oss_path.replace('https://oss.iap.platform.basemind.com/', 's3://'))
            if oss_path:
                with refile.smart_open(oss_path, 'r', encoding='utf-8') as f:
                    txt_list = [i.strip() for i in f.readlines() if i.strip()]
                file_name = oss_path.rstrip('/').split('/')[-1]
        else:
            upload_file = two_col[0].file_uploader("Upload a text file", type=[".txt"])
            if upload_file is not None:
                file_contents = upload_file.read().decode('utf-8')
                txt_list = [i.strip() for i in file_contents.splitlines() if i.strip()]
                file_name = upload_file.name
        two_col[1].text("")
        run_button = two_col[1].button("Run")
        if run_button:
            file_name = get_unquote(f"{os.path.splitext(file_name)[0]}-{model}-{user}")
            model_and_user = [model, user]
            save_oss_path = fr's3://libingx-data/lq/test/tts_to_audio/{now}/{file_name}'
            audio_file_list, txt_file_list, data_list = [], [], []
            for index, txt in enumerate(txt_list, start=1):
                audio_file, txt_file, data = get_audio(model_and_user, txt, config_list)
                audio_file_list.append(audio_file)
                txt_file_list.append(txt_file)
                data_list.append(data)
                print(f"{index}:{txt}")
            df['audio_path']=audio_file_list
            df['txt_path']=txt_file_list
            df['txt']=data_list
            df=mv_audio_and_txt(df, temp_dir, rename_flag)
            df['audio']=df['audio'].apply(lambda x:x.replace(temp_dir, save_oss_path.replace('s3://', 'https://oss.iap.platform.basemind.com/')))     
            refile.smart_sync(temp_dir, save_oss_path)
            _, tar_dir = archive_tar_dir(save_oss_path)
            tar_dir = tar_dir.replace('s3://', 'https://oss.iap.platform.basemind.com/')
            save_oss_path = save_oss_path.replace('s3://', 'https://oss.iap.platform.basemind.com/')
            two_col[0].write(f"audio:{save_oss_path}")
            two_col[0].write(f"tar:{tar_dir}")
            shutil.rmtree(temp_dir)
    if not df.empty:
        st.dataframe(df,use_container_width=True)
    # if not df_process.empty:
    #     three_col=st.columns([1,1])
    #     df_process.index+=1
    #     st.dataframe(df_process)
    #     with three_col[0],three_col[1]:
    #         clicked_row = three_col[0].number_input(f'请输入行号以获取音乐链接:',min_value=1, max_value=len(df_process), step=1)
    #         if 1 <= clicked_row <= len(df):
    #             selected_audio_url=df_process.at[clicked_row, 'audio']
    #             selected_audio_txt=df_process.at[clicked_row,'txt']
    #             three_col[0].markdown(f'<audio src="{selected_audio_url}" controls autoplay></audio>', unsafe_allow_html=True)
    #             three_col[1].text(selected_audio_txt)
                
