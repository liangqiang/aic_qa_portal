import requests
import sys
import os
import shlex
import streamlit as st
from datetime import datetime
import concurrent.futures
import string
import time
# 清除代理设置
os.environ['all_proxy'] = ''
os.environ['no_proxy'] = ''
os.environ['http_proxy'] = ''
os.environ['https_proxy'] = ''
# url = sys.argv[1]
# audio_fn = sys.argv[2]
now = datetime.now().strftime("%Y%m%d")
url='http://arvin.arvinli.basemind-core.svc.platform.basemind.local:23335/asr'
def remove_punctuation_whitespace_special_characters(input_string):
    # 去除标点
    translator = str.maketrans("", "", string.punctuation)
    no_punct = input_string.translate(translator)

    # 去除空格
    no_space = no_punct.replace(" ", "")

    # 去除特殊字符（保留字母和数字）
    no_special_chars = ''.join(char for char in no_space if char.isalnum())

    return no_special_chars
def base_request(params_args,audio_file,root_path):
    print(f'START:{audio_file}-{params_args}')
    params = {
        'root_path': root_path, # 切片服务输出目录，只支持 /data/a10/ 开头的路径
        'separate_vocals': True, # 是否开启人声分离
        'whisper_transcribe': True, # 是否开启 whisper 服务
        'whisper_slice': params_args['whisper_slice'], # 是否根据 whisper 结果做切片服务
        'whisper_annos': True, # 是否输出 whisper 对应的annos.json
        'faster_whisper_transcribe': True, # 是否开启 faster whisper 服务
        'faster_whisper_slice': params_args['faster_whisper_slice'], # 是否根据 faster whisper 结果做切片服务
        'faster_whisper_annos': True,# 是否输出 faster whisper 对应的annos.json
        'funasr_transcribe': True, # 是否开启 funasr 服务
        'funasr_slice': params_args['funasr_slice'], # 是否根据 funasr 结果做切片服务
        'funasr_annos': True # 是否输出 funasr 对应的annos.json
    }
    print(params)
    with open(audio_file, "rb") as file:
        res=""
        for index, _ in enumerate(range(3),start=1):
            try:
                res = requests.post(url, params=params, files={"file": file})
                if 'result' in res.json() and res.json()['result']:
                    print(f"SUCESS{audio_file}:{res.json()}")
                    break  # 如果请求成功，则跳出循环
            except Exception as e:
                print(f'Fail_请求成功无返回结果retry {index}次：{audio_file}:{e}')
        # 在循环结束后检查请求是否成功，如果都失败则打印相应的信息
        if 'result' not in res.json():
            print(f"ERROR_请求失败: {audio_file}")
    return res,audio_file,params_args,root_path
if '__main__' ==__name__:
    st.set_page_config(layout="wide",initial_sidebar_state ='collapsed')
    BASE_PATH = f'/data/a10/lq/{now}' # 只支持 /data/a10/ 开头的路径
    all_task=[]
    one_col=st.columns([8,1,1,1,1,1])
    sucess_list,fail_list,err_list=[],[],[]
    params_args={
        'whisper_slice':True,
        'funasr_slice':True,
        'faster_whisper_slice':True}
    #root_path=os.path.join(BASE_PATH,audio_path.split('/')[-1].split('.')[0])
    with one_col[0],one_col[1],one_col[2],one_col[3],one_col[4],one_col[5]:
        count=0
        FLAG_LIST=['True','False']
        input_path=one_col[0].text_input("audio path","/data/tmp/")
        max_work=int(one_col[1].text_input("work",10))
        whisper_slice_flag=one_col[2].selectbox("whisper",FLAG_LIST)
        funasr_slice_flag=one_col[3].selectbox("funasr",FLAG_LIST)
        faster_whisper_slice_flag=one_col[4].selectbox("faster&whisper",FLAG_LIST[::-1])
        one_col[5].subheader('')
        run_button=one_col[5].button("Start",use_container_width =True)
        params_args['whisper_slice']=whisper_slice_flag
        params_args['funasr_slice']=funasr_slice_flag
        params_args['faster_whisper_slice']=faster_whisper_slice_flag
        
        if os.path.isdir(input_path) and run_button:
            basename=os.path.basename(input_path)
            BASE_PATH=os.path.join(BASE_PATH,basename)
            for root,dirs,files in os.walk(input_path):
                for file in files:
                    if file.endswith(('.wav','.mp3','.m4a')):
                        pre,suff=os.path.splitext(file)
                        #pre=remove_punctuation_whitespace_special_characters(pre)
                        root_path=os.path.join(BASE_PATH,pre)
                        file_path=os.path.join(root,file)
                        all_task.append((params_args,file_path,root_path))
                        count+=1
        elif os.path.isfile(input_path) and run_button:
            count=1
            basename=os.path.basename(input_path)
            #basename=remove_punctuation_whitespace_special_characters(basename)
            pre,suff=os.path.splitext(basename)
            root_path=os.path.join(BASE_PATH,pre)
            file_path=input_path
            all_task.append((params_args,file_path,root_path))
        res_count=0
        with concurrent.futures.ThreadPoolExecutor(max_workers=max_work) as executor:
            futures=[executor.submit(base_request,task[0],task[1],task[2]) for task in all_task]
        for future in concurrent.futures.as_completed(futures):
            res,file,params_args,root_path=future.result()
            if not res:
                err_list.append(file)
            elif res.json()['result']:
                sucess_list.append(file)
                res_count+=1
            elif not res.json()['result']:
                fail_list.append((params_args,file,root_path))
        if fail_list:
            print('---------------------------------------处理FAIL的任务---------------------------------------------')
            time.sleep(10)
            with concurrent.futures.ThreadPoolExecutor(max_workers=max_work) as executor:
                futures=[executor.submit(base_request,task[0],task[1],task[2]) for task in fail_list]
            fail_list=[]
            for future in concurrent.futures.as_completed(futures):
                res,file,params_args,root_path=future.result()
                if not res:
                    err_list.append(file)
                elif not res.json()['result']:
                    fail_list.append(file)

    if sucess_list:
        sucess_list=[f"SUCESS:{i}" for i in sucess_list]
        st.text("\n".join(sucess_list))
        print(sucess_list)
    if fail_list:
        fail_list=[f"FAIL_请求成功无返回结果:{i}" for i in fail_list]
        st.text("\n".join(fail_list))
        print(fail_list)
    if err_list:
        err_list=[f"ERROR_请求失败:{i}" for i in err_list]
        st.text("\n".join(err_list))
        print(err_list)
            
    #base_request(audio_path,root_path)

