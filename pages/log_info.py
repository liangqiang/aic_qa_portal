import streamlit as st
import requests
import json
import pandas as pd


if '__main__' ==__name__:
    table_list=['dataframe','lable']
    BASE_URL='http://180.184.175.29:28000/load_session/sHiT4hnYzm3XcK1L'
    st.set_page_config(layout="wide",initial_sidebar_state ='collapsed')
    one_col=st.columns([1])
    url=one_col[0].text_input("请输入链接",BASE_URL)
    two_col=st.tabs( table_list)
    #st.text(url)
    response = requests.get(url)
    txt=response.text
    json_data=json.loads(txt)
    log_data=json_data['logs']
    dict_log={}
    dict_key={}
    other_info_log=[]
    for i in (log_data):
        for k,v in i.items():
            try:
                v=json.loads(v)
                dict_log[k]=v
                if k.rsplit('_',1)[0] not in dict_key:
                    dict_key[k.rsplit('_',1)[0]]=[{k:v}]
                else:
                    dict_key[k.rsplit('_',1)[0]].append({k:v})
            except Exception as e:
                v=v
                other_info_log.append(v)
    with two_col[0]:
        for k,b in dict_key.items():
            test=[]
            keys=[]
            for i in b:
                for k2,v2 in i .items():
                    test.append(v2)
                    keys.append(k2)
                # print(b)
            df=pd.DataFrame(test,index=keys).T
            #print(test)
            st.title(f'{keys[0]}  VS  {keys[1]}')
            #with st.expander(f'{keys[0]}  VS  {keys[1]}') :
            st.dataframe(df,use_container_width=True)
    with two_col[1]:
        for k,b in dict_key.items():
            test=[]
            keys=[]
            for i in b:
                for k2,v2 in i .items():
                    test.append(v2)
                    keys.append(k2)
                # print(b)
            df=pd.DataFrame(test,index=keys).T
            #print(test)
            st.title(f'{keys[0]}  VS  {keys[1]}')
            #with st.expander(f'{keys[0]}  VS  {keys[1]}') :
            st.table(df)
        #break
