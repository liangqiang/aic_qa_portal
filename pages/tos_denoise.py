from functools import partial
import json
from loguru import logger
import refile
import os
import pandas as pd
from pathlib import Path
import random
import streamlit as st


BASE_DIR = Path(__file__).resolve().parent

if "data" not in st.session_state:
    st.session_state.data = {}


def bilibili_name(dest_name: str):
    return dest_name.replace(
        DATASETS["bilibili"]["dest"], DATASETS["bilibili"]["src"]
    ).replace("audio.wav", "audio.m4s")


def bilibili_name2(dest_name: str, key=str):
    return dest_name.replace(DATASETS[key]["dest"], DATASETS[key]["src"]).replace(
        ".wav", ".m4s"
    )


def youtube_name(dest_name: str):
    return dest_name.replace(
        DATASETS["youtube"]["dest"], DATASETS["youtube"]["src"]
    ).replace("/audio.wav", ".mp3")


DATASETS = {
    "bilibili": {
        "src": "s3://a-collections-sh/data/生活社区类/哔哩哔哩/video",
        "dest": "s3://mby-public/bilibili",
        "func": bilibili_name,
    },
    "bilibili-mooc": {
        "src": "s3://a-collections-sh/data/生活社区类/bilibili-mooc",
        "dest": "s3://mby-public/denoise/bilibili/bilibili-mooc",
        "func": partial(bilibili_name2, key="bilibili-mooc"),
    },
    "bilibili-20231124": {
        "src": "s3://a-collections-sh/data/生活社区类/bilibili/bilibili-20231124",
        "dest": "s3://mby-public/denoise/bilibili/bilibili-20231124",
        "func": partial(bilibili_name2, key="bilibili-20231124"),
    },
    "youtube": {
        "src": "s3://mby-public/youtube/tedx",
        "dest": "s3://mby-public/denoise/youtube",
        "func": youtube_name,
    },
}


def oss2https(oss: str):
    if oss.startswith(DATASETS["youtube"]["src"]):
        return oss.replace(
            DATASETS["youtube"]["src"],
            "https://oss.iap.platform.basemind.com/nlp-data-map/audio",
        )
    else:
        p = Path(oss.replace("s3://", "")).parent
        dirs = str(p).split("/")
        id = dirs.pop(0)
        prefix = "%2F".join(dirs)
        return f"https://console.volcengine.com/tos/bucket/setting?type=objects&region=cn-shanghai&id={id}&dirPrefix={prefix}%2F"


def init_env():
    pass
    # # proxy config
    # try:
    #     with open(f"{BASE_DIR}/../env/proxy_config.txt", "r") as f:
    #         data = f.read()
    #         if data:
    #             proxy_config = json.loads(data)
    #             os.environ["all_proxy"] = proxy_config["all_proxy"]
    #             os.environ["no_proxy"] = proxy_config["no_proxy"]
    #             os.environ["http_proxy"] = proxy_config["http_proxy"]
    #             os.environ["https_proxy"] = proxy_config["https_proxy"]
    # except:
    #     logger.error(f"read {BASE_DIR}/env/proxy_config.txt error")

    #tos config
    # access_key_id = "AKLTMTYwNTUwNmJjYWM0NDUwY2FiOTBmZTk0ODY5ZjEzODY"
    # secret_access_key = "TURKaU5HSmhPV1ZpTlRjeE5HVTFNV0kyT0RKaU16WTFabVZpTWpRM1lqRQ=="
    # os.environ["AWS_ACCESS_KEY_ID"] = access_key_id
    # os.environ["AWS_SECRET_ACCESS_KEY"] = secret_access_key
    # os.environ["OSS_ENDPOINT"] = "http://tos-s3-cn-shanghai.ivolces.com"
    # os.environ["AWS_S3_ADDRESSING_STYLE"] = "virtual"


def read_audio(oss: str, length: int = 30 * 1024 * 1024):
    content = b""
    with refile.smart_open(oss, "rb") as fr:
        content = fr.read(length)
    return content


# 使用st.cache确保此函数只在首次调用时运行
@st.cache_resource
def load_audios():
    audios = {}
    for key, value in DATASETS.items():
        audios[key] = refile.smart_iglob(f"{value['dest']}/**/*.wav")

    return audios


def layout():
    st.set_page_config(layout="wide", initial_sidebar_state="collapsed")
    audios = load_audios()
    max_items = 10

    key = st.selectbox(
        "请选择数据集:",
        tuple(DATASETS.keys()),
        placeholder="Select dataset...",
    )
    random_button = st.button("🎲: 换批数据")
    if key and random_button:
        st.session_state.data[key] = []
        for _ in range(max_items):
            try:
                random_number = random.randint(50, 100)
                for _ in range(random_number):
                    next(audios[key])
                file_name = next(audios[key])
            except StopIteration:
                audios[key] = refile.smart_iglob(f"{DATASETS[key]['dest']}/**/*.wav")
                file_name = next(audios[key])
            st.session_state.data[key].append(file_name)

    if key and key in st.session_state.data:
        df = pd.DataFrame(
            {
                "index": [n + 1 for n in range(max_items)],
                "origin": [
                    oss2https(DATASETS[key]["func"](fn))
                    for fn in st.session_state.data[key]
                ],
                "denoise": [oss2https(fn) for fn in st.session_state.data[key]],
            }
        )
        st.dataframe(
            df,
            column_config={
                "index": st.column_config.NumberColumn("行号"),
                "origin": st.column_config.LinkColumn("降噪前"),
                "denoise": st.column_config.LinkColumn("降噪后"),
            },
            hide_index=True,
            use_container_width=True,
        )
        row = st.number_input(
            f"请输入行号加载音频播放",
            min_value=0,
            max_value=len(st.session_state.data[key]),
            step=1,
            key=key,
            value=0,
        )
        if row:
            denoise_fn = st.session_state.data[key][row - 1]
            origin_fn = DATASETS[key]["func"](denoise_fn)
            denoise_fmt = Path(denoise_fn).suffix
            origin_fmt = Path(origin_fn).suffix
            denoise_audio = read_audio(denoise_fn)
            origin_audio = read_audio(origin_fn)
            st.write(st.session_state.data[key][row - 1])
            col1, col2 = st.columns(2)
            with col1:
                st.write("降噪前")
                st.write("降噪后")
            with col2:
                st.audio(origin_audio, format=f"audio/{origin_fmt}")
                st.audio(denoise_audio, format=f"audio/{denoise_fmt}")


if __name__ == "__main__":
    init_env()
    layout()
