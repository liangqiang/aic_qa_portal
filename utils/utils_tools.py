import os
import numpy as np
import pandas as pd
from pandas.api.types import is_numeric_dtype

# from pandas_profiling import ProfileReport
from collections import defaultdict
import gitlab
import refile
import tabulate
import cv2
import streamlit as st
from IPython import embed

TEAM_MEMBERS = {
    "libingxin": {
        "name": "李冰心",
    },
    "a-titan": {
        "name": "梁强",
    },
    "sunpeiqin": {
        "name": "孙培钦",
    },
    "lizheng02":{
        "name":"李峥"
    },
    "zhouhebin":{
    "name":"周赫斌"}
    ,
    "heqinglin":{
        "name":"何青林"
    },
    "yuansong":{
        "name":"袁嵩"
    }
}

BMK_LINKS_JSON = "./bmk_link.json"


def get_gitlab(site="pd"):
    token_file, url = {
        "pd": (".gittoken-pd", "https://git-pd.megvii-inc.com/"),
        "core": (".gittoken-core", "https://git-core.megvii-inc.com/"),
    }[site]
    token = open(token_file).read().strip()
    #print(site,token)
    gl = gitlab.Gitlab(url, private_token=token)
    gl.auth()
    return gl


def get_issue_infos():
    group_name = {257: "tidbit", 2719: "tidbit_compiler", 420: "FpgaTeam"}
    site_groups = {
        "pd": [257, 2719],
        "core": [420],
    }
    assignee_issues = defaultdict(list)

    i = 0
    for site in site_groups:
        print(site)
        gl = get_gitlab(site=site)
        for group_id in site_groups[site]:
            group = gl.groups.get(group_id)
            #print(group)
            issues = group.issues.list(all=True, state="opened")
            st.text(
                "Processing site:git-{} group: {}".format(site, group_name[group_id])
            )
            my_bar = st.progress(0)
            i = 0
            for issue in issues:
                if issue.assignee is not None:
                    assignee = issue.assignee["username"]
                    if assignee in TEAM_MEMBERS:
                        assignee_issues[assignee].append(
                            {
                                "title": issue.title,
                                "web_url": issue.web_url,
                                "has_tasks": issue.has_tasks,
                                "task_completion_status": issue.task_completion_status,
                            }
                        )
                my_bar.progress(int((i + 1) / len(issues) * 100))
                i += 1
    return assignee_issues


def get_issue_infos_reporter():
    group_name = {257: "tidbit", 2719: "tidbit_compiler", 420: "FpgaTeam"}
    site_groups = {
        "pd": [257, 2719],
        "core": [420],
    }
    reporter_issues = defaultdict(list)

    i = 0
    for site in site_groups:
        gl = get_gitlab(site=site)
        for group_id in site_groups[site]:
            group = gl.groups.get(group_id)
            issues = group.issues.list(all=True, state="opened")
            st.text(
                "Processing site:git-{} group: {}".format(site, group_name[group_id])
            )
            my_bar = st.progress(0)
            i = 0
            for issue in issues:
                # print(issue)
                if issue.author is not None:
                    reporter = issue.author["username"]
                    if reporter in TEAM_MEMBERS:
                        reporter_issues[reporter].append(
                            {
                                "title": issue.title,
                                "web_url": issue.web_url,
                                "has_tasks": issue.has_tasks,
                                "task_completion_status": issue.task_completion_status,
                            }
                        )
                my_bar.progress(int((i + 1) / len(issues) * 100))
                i += 1
    return reporter_issues


def get_issue_table(issue_infos):
    header = ["Name", "Issue Number", "Issue List", "Task Status"]
    table = []
    fmt = ["s", "s", "s", "s"]
    for user_name, info in TEAM_MEMBERS.items():
        if user_name in issue_infos:
            issue_info = issue_infos[user_name]
            name = info["name"]
            table.append(
                (
                    name,
                    len(issue_info),
                    "<br>".join(
                        [
                            "[{}]({})".format(i["title"], i["web_url"])
                            for i in issue_info
                        ]
                    ),
                    "<br>".join(
                        [
                            "{}/{}".format(
                                i["task_completion_status"]["completed_count"],
                                i["task_completion_status"]["count"],
                            )
                            for i in issue_info
                        ]
                    ),
                )
            )
    table_info = tabulate.tabulate(table, header, floatfmt=fmt, tablefmt="pipe")
    return table_info



def make_oss_show_link(
    link, oss_iap_head="https://oss.iap.hh-b.brainpp.cn/", s3_head="s3://"
):
    if link == "":
        return link
    if link[: len(s3_head)] == s3_head:
        link = link.replace(s3_head, oss_iap_head)
    assert (
        link[: len(oss_iap_head)] == oss_iap_head
    ), "Please make sure you use iap.hh-b for image show"
    return link


def make_oss_s3_link(
    link, oss_iap_head="https://oss.iap.hh-b.brainpp.cn/", s3_head="s3://"
):
    if link == "":
        return link
    if link[: len(oss_iap_head)] == oss_iap_head:
        link = link.replace(oss_iap_head, s3_head)
    assert (
        link[: len(s3_head)] == s3_head
    ), "Please make sure you set oss bucket starting with `s3://`"
    return link


def filter_by_suffixs(fnames, suffixs):
    fnames = [fname for fname in fnames if os.path.splitext(fname)[1] in suffixs]
    return fnames


def get_oss_coexist_fname(
    oss_dir1, oss_dir2, oss_dir3, suffixs=[".jpg", ".JPG", ".png", ".PNG"]
):
    flags = []
    if oss_dir1 != "":
        dir1_names = refile.smart_listdir(oss_dir1)
        dir1_names = filter_by_suffixs(dir1_names, suffixs)
        fnames = set(dir1_names)
        flags.append(True)
    if oss_dir2 != "":
        dir2_names = refile.smart_listdir(oss_dir2)
        dir2_names = filter_by_suffixs(dir2_names, suffixs)
        fnames = fnames & set(dir2_names)
        flags.append(True)
    if oss_dir3 != "":
        dir3_names = refile.smart_listdir(oss_dir3)
        dir3_names = filter_by_suffixs(dir3_names, suffixs)
        fnames = fnames & set(dir3_names)
        flags.append(True)
    return fnames, flags


def get_oss_basename(oss_dir):
    if oss_dir[-1] == "/":
        oss_dir = oss_dir[:-1]
    return os.path.basename(oss_dir)


def image_resize(img, resize_scale=1):
    img = cv2.resize(
        img, (int(img.shape[1] * resize_scale), int(img.shape[0] * resize_scale))
    )
    return img


def smart_load_image(img_path, to_rgb=False):
    img = refile.smart_load_image(img_path)
    if to_rgb:
        img = img[:, :, ::-1]
    return img
