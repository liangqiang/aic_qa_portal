import imp
import io
import os ,json,time
import pymongo
import streamlit as st
from st_aggrid import AgGrid, GridOptionsBuilder
import base64
from refile import smart_open,smart_listdir, smart_getsize,smart_exists, smart_sync, smart_remove,smart_glob,smart_copy,smart_stat,smart_isdir,smart_isfile,smart_walk
import pandas as pd
import librosa
from pydub import AudioSegment
from tarfile import open as tar_open, TarInfo, REGTYPE, DIRTYPE
from refile import smart_open,smart_walk,smart_path_join,smart_getsize,smart_remove
import refile
import urllib.parse
import numpy as np
import threading
import subprocess
import tempfile
import re
import shlex
from loguru import logger
from pathlib import Path
import concurrent.futures
from tqdm import tqdm
def get_boradlist():
    data_acc_qa=os.popen("remote-board list-tags --group aic-qa").read()
    data=os.popen("remote-board list-tags").read()
    data=json.loads(data)
    data_acc_qa=json.loads(data_acc_qa)
    return data,data_acc_qa

def get_time(f):
    def inner(*arg,**kwarg):
        s_time = time.time()
        res = f(*arg,**kwarg)
        e_time = time.time()
        dur_time=e_time - s_time
        print('耗时：{}秒'.format(dur_time))
        return dur_time
    return inner


def get_mongo_db(ppl_id):
    mongo_client = pymongo.MongoClient('mongodb://qa:tidbit-qa@pacific-mongodb-aic.mcd.megvii-inc.com:27077/?authSource=pacific')
    print(mongo_client)
    db_pacific=mongo_client['pacific']
    #db_pacific.list_collection_names() 获取db_pacific中所有集合
    collection_pacific=db_pacific['report_ax']
    pacific_data=collection_pacific.find({"pipeline_id":int(ppl_id)}) 
    return pacific_data                        

def get_lib_so(borad_type):
    so_list=smart_glob(f's3://pacific/tools/joint_sdk/*.{f"{borad_type}"}*')
    run_joint_list=smart_glob(f's3://pacific/tools/joint_sdk/*.{f"{borad_type}"}*')
    so_list=[i.split('joint_sdk/')[-1] for i in so_list if "libax_run_joint" in i]
    run_joint_list=[i.split('joint_sdk/')[-1] for i in run_joint_list if "run_joint" in i]
    return so_list,run_joint_list
#@st.cache
def from_oss_html_get_dataframe(oss_url:str):
    html_data=smart_open(oss_url)
    return pd.read_html(html_data)

@st.cache_resource
def render_ag_grid(data_frame):
    # 重置索引
    dataframe = data_frame.reset_index()
    #print(data_frame)
    # 创建GridOptionsBuilder对象，并配置复选框选项
    builder = GridOptionsBuilder.from_dataframe(dataframe)
    builder.configure_selection(selection_mode="multiple",use_checkbox=True)
    builder.configure_column('index', headerName='', headerCheckboxSelection=True, headerCheckboxSelectionFilteredOnly=True, headerCheckboxSelectionCurrentPageOnly=True)
    #builder.configure_pagination(paginationPageSize=10,pagination=True )
    #grid_options['pagination'] = True
    # 构建ag-Grid的选项
    go = builder.build()
    go['paginationPageSize']=30
    go['pagination']=True
    return dataframe,go
def download_csv(df,name,save_file_name):
    csv_file = df.to_csv(encoding='utf-8-sig')
    b64 = base64.b64encode(csv_file.encode('utf-8-sig')).decode()  # 将CSV文件编码为base64字符串
    return f'<a href="data:file/csv;base64,{b64}" download="{name}.csv">下载{save_file_name} CSV文件</a>'
def download_xlsx(df,name,save_file_name):
    csv_file = df.to_csv(encoding='utf-8-sig')
    b64 = base64.b64encode(csv_file.encode('utf-8-sig')).decode()  # 将CSV文件编码为base64字符串
    return f'<a href="data:file/xlsx;base64,{b64}" download="{name}.xlsx">下载{save_file_name} xlsx</a>'
# @st.cache(suppress_st_warning=True)
# def clicked():
#     return True

def download_file(file_path):
    with open(file_path, "rb") as f:
        file_data = f.read()
        file_name = os.path.basename(file_path)  # 提取文件名

        # 使用 st.markdown 提供下载链接
        b64_data = base64.b64encode(file_data).decode()
        href = f'<a href="data:application/octet-stream;base64,{b64_data}" download="{file_name}">{file_name}</a>'
    return href
def get_dataframe_tar_from_oss(oss_path,audio_flag):
    from restore import archive_open, video_open
    from tqdm import tqdm
    wav_dict={}
    txt_dict={}
    with archive_open(oss_path) as reader:
        for archive in tqdm(reader):
            #print(archive.name)  # 压缩包里文件的文件名
            # archive.stat.size  # 文件大小
            # archive.stat.mtime  # 修改时间
            if ".txt" in archive.name and audio_flag:
                decoded_string = archive.read().decode('utf-8')
                #print(decoded_string)
                txt_dict[archive.name]=str(decoded_string)
            elif  archive.name.endswith('.wav') or archive.name.endswith('.mp3') or archive.name.endswith('.m4a'):
                wav_dict[archive.name]=archive.read()
                if not audio_flag:
                    txt_dict[archive.name]='NA'
    wav_dict = dict(sorted(wav_dict.items()))
    txt_dict=dict(sorted(txt_dict.items()))
    print(len(wav_dict))
    print(len(txt_dict))
    return wav_dict,list(txt_dict.values())

def is_punctuation(text):
    # 使用正则表达式匹配只包含中文标点和英文标点的字符串
    pattern = re.compile(r'^[，。、！？《》「」【】【】（）“”‘’\'\"\"【】.,;:?!-]+$')
    return bool(pattern.match(text))
# def check_empty_audio(audio_path):
#     try:
#         if os.path.isfile(audio_path):
#             audio = AudioSegment.from_file(audio_path)
#         else :
#             with refile.smart_open(audio_path, 'rb') as f:
#                 audio_data = f.read()
#             audio = AudioSegment.from_file(io.BytesIO(audio_data))
#         y = np.array(audio.get_array_of_samples())
#         energy = sum(abs(y)**2)
#         #print(f"{audio_path}:{energy}")
#     except Exception as e:
#         print(f"{audio_path}:{e}")
#         return True
#         # 设置一个阈值，用于判断音频是否为空
#     threshold = 0.0001  # 根据需要调整阈值
#  return energy ==0,audio_path
def check_empty_audio(audio_path):
    try:
        audio = AudioSegment.from_file(audio_path)
        duration_in_seconds = audio.duration_seconds
        return duration_in_seconds==0,audio_path
    except Exception as e:
        print(f"{audio_path}:{e}")
        return True,audio_path
def audio_process(path):
    rm_file_list=[]
    all_task=[]
    rm_audio_count=0
    for root,dirs,files in smart_walk(path):
        for file in files:
            if file.endswith(".txt") :
                path_txt=refile.smart_path_join(root,file)
                with smart_open(path_txt,'r') as f:
                    txt=f.read().strip().replace('[ZH]', '').strip()
                    if not txt or is_punctuation(txt):
                        rm_file_list.append(path_txt)
                        rm_file_list.append(path_txt.replace('.txt', '.wav'))

    #         elif file.endswith(('.wav','.mp3','.m4a'))  :
    #             audio_file=refile.smart_path_join(root,file)
    #             all_task.append(audio_file)
    #             # if check_empty_audio(audio_file):
    #             #     rm_file_list.append(audio_file)
    #             #     rm_file_list.append(audio_file.replace("."+file.rsplit('.')[-1], '.txt'))
    # with concurrent.futures.ThreadPoolExecutor(max_workers=int(5)) as executor: 
    #     futures=[executor.submit(check_empty_audio,task)for task in all_task]
    # for future in tqdm(concurrent.futures.as_completed(futures)):
    #     flag,file=future.result()
    #     if flag:
    #         rm_file_list.append(file)
    #         rm_file_list.append(file.replace("."+file.rsplit('.')[-1], '.txt'))
    for file in set(rm_file_list):
        print(file)
        rm_audio_count+=1
        smart_remove(file)
    return rm_audio_count
def time_to_minutes(time_str):
    # 将时间字符串拆分为小时、分钟和秒
    hours, minutes, seconds = map(float, time_str.split(':'))

    # 将小时转换为分钟
    total_minutes = hours * 60

    # 加上分钟和秒
    total_minutes += minutes + seconds / 60

    return total_minutes
# def audio_dur_from_binary(audio_data,type):
#     try:
#         if type=='S3 Path':
#             with refile.smart_open(audio_data, 'rb') as f:
#                 #print('VVVVVVVVVVVVVV')
#                 data = f.read()
#                 f.close()
#             audio_data=io.BytesIO(data)
#         y, sr = librosa.load(audio_data)
#         # 获取音频时长（以秒为单位）
#         duration_seconds = librosa.get_duration(y=y, sr=sr)
#         duration_seconds = duration_seconds / 60
#         return duration_seconds,None
#     except Exception as e:
        
#         print(f"发生未知异常:{audio_data}- {e}")
#         return 0,audio_data
#     # finally:
#     # # 在这里添加释放资源的代码
#     #     if 'audio' in locals():
#     #         audio.close()
def audio_dur_from_binary(audio_name,data,type=""):
    #audio_name_quoted =shlex.quote(audio_name)
    
    try:
        if not isinstance(data,str):
            
            with tempfile.NamedTemporaryFile(delete=False,dir='/data/tmp/audio_tmp') as temp_input_file:
                temp_input_file.write(data)
                audio_data = temp_input_file.name 
        elif type=='S3 Path':

            with refile.smart_open(audio_name, 'rb') as f:
                binary_audio_data = f.read()
            with tempfile.NamedTemporaryFile(delete=False,dir='/data/tmp/audio_tmp') as temp_input_file:
                temp_input_file.write(binary_audio_data)
                audio_data = temp_input_file.name
        else:
            audio_data=audio_name
        
        command = f'ffmpeg -i {shlex.quote(audio_data)} 2>&1 | grep Duration'
        result = subprocess.run (command, shell=True,  text=True, check=True,stdout=subprocess.PIPE)
        duration_match = re.search(r'Duration: (\d+):(\d+):(\d+\.\d+)', result.stdout)
        hours, minutes, seconds = map(float, duration_match.groups())
        duration_seconds = hours * 3600 + minutes * 60 + seconds
        if not isinstance(data,str) or type=='S3 Path':
            os.remove(audio_data)

        return duration_seconds,None
    # except pydub.exceptions.CouldntDecodeError as decode_error:
    #     print(f"解码音频时出错: {decode_error}")
    except Exception as e:
        print(f"发生未知异常:{audio_name}- {e}")
        return 0,audio_name
    # finally:
    #     # 删除临时文件
    #     os.remove(temp_input_file_path)
def tar_file(basename,filename,f,relative):
    file_=smart_path_join(basename, filename)
    with smart_open(file_, 'rb') as r:
        info = TarInfo(smart_path_join(relative, filename))
        info.size = smart_getsize(smart_path_join(basename, filename))
        #total_size+=info.size
        info.type = REGTYPE
        #info.mode = 0o777
        f.addfile(info, fileobj=r)    
    return info.size

### 压缩文件夹
def archive_tar_dir(SRC,tar_name):
    total_size=0
    all_task=[]
    print(tar_name)
    if SRC.endswith('/'):
        SRC=SRC.rstrip('/')
    if tar_name:
        parent_directory = "/".join(SRC.split("/")[:-1])
        #TAR_NAME=f'{refile.smart_path_join(parent_directory,tar_name)}.tar.gz'
        TAR_NAME=f'{refile.smart_path_join(parent_directory,tar_name+"/"+tar_name)}.tar.gz'
    else:
        TAR_NAME=f'{SRC}.tar.gz'
    with smart_open(TAR_NAME, 'wb') as w:
        
        f = tar_open(fileobj=w, mode='w:gz')
        for basename, dirs, files in smart_walk(SRC):
            relative = basename[len(SRC):]  # remove base of base
            # print(basename)
            # print(relative)
            for dirname in dirs:
                info = TarInfo(smart_path_join(relative, dirname))
                info.type = DIRTYPE
                info.mode = 0o777
                f.addfile(info)
            for filename in tqdm(files):
                all_task.append(((basename,filename,f,relative)))
                # with smart_open(smart_path_join(basename, filename), 'rb') as r:
                #     info = TarInfo(smart_path_join(relative, filename))
                #     info.size = smart_getsize(smart_path_join(basename, filename))
                #     total_size+=info.size
                #     info.type = REGTYPE
                #     #info.mode = 0o777
                #     f.addfile(info, fileobj=r)
        with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
            futures=[executor.submit(tar_file,task[0],task[1],task[2],task[3]) for task in tqdm(all_task)]
        for future in futures:
            info_size=future.result()
            total_size+=info_size
        f.close()
    return total_size,TAR_NAME


### 压缩文件
def archive_tar(SRC,tar_name):
    total_size=0
    info_data=SRC.split('/')[-1]
    if tar_name:
        #parent_directory = "/".join(SRC.split("/")[:-1])
        TAR_NAME=f'{refile.smart_path_join(SRC,tar_name)}.tar.gz'
    else:
        TAR_NAME=f'{SRC}.tar.gz'
    #TAR_NAME=f'{SRC}.tar.gz'
    with smart_open(SRC, 'rb') as r:
        with smart_open(TAR_NAME, 'wb') as w:
            f = tar_open(fileobj=w, mode='w:gz')
            info = TarInfo(info_data)
            info.size = smart_getsize(SRC)
            info.type = REGTYPE
            info.mode = 0o644
            total_size=info.size
            f.addfile(info, fileobj=r)
            f.close()
    return total_size,TAR_NAME


def cut_and_replace_audio(input_path, timestamp, output):
    start, end = timestamp.split('-')
    start_time = sum(x * int(t) * 1000 for x, t in zip([3600, 60, 1], start.split(':')))
    end_time = sum(x * int(t) * 1000 for x, t in zip([3600, 60, 1], end.split(':')))

    # 读取原始音频文件
    original_audio = AudioSegment.from_file(input_path)

    # 切割音频
    part1 = original_audio[:start_time]
    part2 = original_audio[end_time:]

    # 合并切割后的部分
    modified_audio = part1 + part2

    # 将生成的音频输出到文件
    modified_audio.export(os.path.join(output, input_path.rsplit('/')[-1]), format=input_path.rsplit('.', 1)[-1])

    return input_path, [start_time / 1000, end_time / 1000]

def get_unquote(oss_path):
    oss_path = urllib.parse.unquote(oss_path)
    return oss_path
### 生成1器
def audio_files_generator(all_task):
    for task in all_task:
        yield task
        
def init_tos_env():
    BASE_DIR = Path(__file__).resolve().parent
    # proxy config
    try:
        with open(f"{BASE_DIR}/../env/proxy_config.txt", "r") as f:
            data = f.read()
            if data:
                proxy_config = json.loads(data)
                os.environ["all_proxy"] = proxy_config["all_proxy"]
                os.environ["no_proxy"] = proxy_config["no_proxy"]
                os.environ["http_proxy"] = proxy_config["http_proxy"]
                os.environ["https_proxy"] = proxy_config["https_proxy"]
    except:
        logger.error(f"read {BASE_DIR}/env/proxy_config.txt error")

    #tos config
    access_key_id = "AKLTMTYwNTUwNmJjYWM0NDUwY2FiOTBmZTk0ODY5ZjEzODY"
    secret_access_key = "TURKaU5HSmhPV1ZpTlRjeE5HVTFNV0kyT0RKaU16WTFabVZpTWpRM1lqRQ=="
    os.environ["AWS_ACCESS_KEY_ID"] = access_key_id
    os.environ["AWS_SECRET_ACCESS_KEY"] = secret_access_key
    os.environ["OSS_ENDPOINT"] = "http://tos-s3-cn-shanghai.ivolces.com"
    os.environ["AWS_S3_ADDRESSING_STYLE"] = "virtual"
    
def init_basemind_oss_env():
    BASE_DIR = Path(__file__).resolve().parent
    #tos config
    access_key_id = "47e37a7aa627935ae1bc5f0f3925f08d"
    secret_access_key = "7147feb83c621be7f1d6cd423eb02213"
    os.environ["AWS_ACCESS_KEY_ID"] = access_key_id
    os.environ["AWS_SECRET_ACCESS_KEY"] = secret_access_key
    os.environ["OSS_ENDPOINT"] = "http://oss.i.basemind.com"
    os.environ["AWS_S3_ADDRESSING_STYLE"] = "virtual"