import pandas as pd
import os


def read_txt(path_txt):
    with open(path_txt,'r',encoding='utf-8') as f:
        data=f.read()
    #print(data)
    return data.strip()

def get_lable_excel(path):
    
    file_list=os.listdir(path)
    wav_list=sorted([i for i in file_list if i.endswith(('.wav','.mp3','.wav'))])
    #print(wav_list)
    txt_list=sorted([os.path.join(path,i) for i in file_list if i.endswith('.txt')])
    wav_path=[os.path.join(path,i) for i in wav_list]
    print(len(wav_list))
    print(len(txt_list))
    print(len(wav_path))
    # df=pd.DataFrame()
    # df['wav']=wav_list
    # df['txt']=txt_list
    #df['txt']=df['txt'].apply(read_txt)
    #df.to_excel(path+'.xlsx')
    return wav_list, txt_list,wav_path
if '__main__' ==__name__:
    path=r'/data/tmp/PM100_guoyu_pairs2'
    all_wav=[]
    all_txt=[]
    all_wav_path=[]
    df=pd.DataFrame()
    for root ,dirs,files in os.walk(path):
        if dirs:
            wav_list,txt_list,wav_path=[],[],[]
            for dir in dirs:
                if dir=="whisper":
                    print(os.path.join(root,dir))
                    wav_list, txt_list, wav_path=get_lable_excel(os.path.join(root,dir))
                    all_wav+=wav_list
                    all_txt+=txt_list
                    all_wav_path+=wav_path
    df['wav']=all_wav
    df['txt']=all_txt
    df['path']=all_wav_path
    df['txt']=df['txt'].apply(read_txt)
    print(path)
    df.to_excel(path+'.xlsx')