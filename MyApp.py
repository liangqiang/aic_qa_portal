import streamlit as st
import pandas as pd
import numpy as np
import datetime
from IPython import embed




# Custom imports
from multipage import MultiPage
from pages import (
    json_to_excel,
    log_info,
    test_qa,
    audio_release,
    small_tool,
    audio_slice_denonise,
    audio_label,
    tts_to_audio,
    tos_basemind,
    tos_play_audio,
    tos_denoise,
    whisper_funasr_slice_denoise
)

favicon = "resource/banana_1f34c.png"
st.set_page_config(
    page_title="Welcome to AIC-QA",
    page_icon=favicon,
    layout="wide",
    #initial_sidebar_state="auto",
)

app = MultiPage(
    "https://gitlab.basemind.com/basemind/aic_qa_portal/-/project_members"
)


st.sidebar.title("Welcome to AIC-QA 😀")
st.sidebar.write("[json to excel](json_to_excel)")
st.sidebar.write("[log info](log_info)")
st.sidebar.write("[whisper funaser slice denoise](whisper_funasr_slice_denoise)")
st.sidebar.write("[audio slice denonise](audio_slice_denonise)")
st.sidebar.write("[audio label](audio_label)")
st.sidebar.write("[audio release](audio_release)")
st.sidebar.write("[small tool](small_tool)")
st.sidebar.write("[tts to audio](tts_to_audio)")
st.sidebar.write("[tos  basemind](tos_basemind)")
st.sidebar.write("[tos  play audio](tos_play_audio)")
st.sidebar.write("[tos  denoise](tos_denoise)")


app.add_page("QA",test_qa.app)
app.run()
