
import sys 
import ast
sys.path.append('/data/liangqiang/QA/aic_qa_portal/') 
from utils import general
import concurrent.futures
from tqdm import tqdm
# 添加上一级目录到系统路径中
all_task = sys.argv[1]

def main(all_task):
    all_task = ast.literal_eval(all_task)
    batch_size = 50
    #print(batch_size)
    err_audio_list = []
    total_duration = 0  # 初始化总时长
    count = 0  # 初始化音频总数
    error_count = 0  
    batch_size = 50  # 根据系统资源和任务需求调整批次大小
    for i in range(0, len(all_task), batch_size):
        batch = all_task[i:i + batch_size]
        audio_generator = general.audio_files_generator(batch)
        with concurrent.futures.ProcessPoolExecutor(max_workers=5) as executor:
            futures = [executor.submit(general.audio_dur_from_binary, task[0], task[1], task[2]) for task in tqdm(audio_generator, desc=f"Processing audio files: {len(batch)}条")]
            #concurrent.futures.wait(futures)
            for future in concurrent.futures.as_completed(futures):
                try:
                    duration, error_audio = future.result()
                    print(duration)
                    count += 1
                    if error_audio:
                        error_count += 1
                        err_audio_list.append(error_audio)
                    total_duration += duration
                except Exception as e:
                    print(f"Error in processing audio file: {e}")
                    # 这里你可以打印异常信息以帮助定位问题
    return total_duration, count, error_count, err_audio_list

if '__main__' == __name__:
    main(all_task)