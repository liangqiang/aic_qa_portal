import sys
import os
import subprocess
import time
import refile
import concurrent.futures
import urllib.request
import argparse
import requests
import streamlit as st
from datetime import datetime
import tempfile
import sys
now = datetime.now().strftime("%Y%m%d")
start_time=time.time()
os.environ['all_proxy'] = ''
os.environ['no_proxy'] = ''
os.environ['http_proxy'] = ''
os.environ['https_proxy'] = ''
dir_name = sys.argv[1]
type_model=sys.argv[2]
def get_unquote(oss_path):
    oss_path = urllib.parse.unquote(oss_path)
    return oss_path
def run_job(root_path,method,fn):
    for index,i in enumerate(range(5),start=1):
        
        try:
            if method=="funasr":
                #url='http://denji.denjiliang.basemind-core.svc.platform.basemind.local:23333/asr'
                url='http://10.144.0.76:23333/asr'
                res = requests.post(f"{url}?root_path={root_path}&slice_method={method}", files={"file": open(fn, "rb")})
            elif method=="whisper":
                url='http://10.144.0.60:23333/asr'
                #url='http://denji.denjiliang.basemind-core.svc.platform.basemind.local:23334/asr'
                res = requests.post(f"{url}?root_path={root_path}", files={"file": open(fn, "rb")})
            if 'result' in res.json() and res.json()['result']:
                print(f"{method}-命令执行成功: {res.json()}:{fn}")
                break  # 如果请求成功，则跳出循环
            else:
                print(f'retry:{index}次,{fn}')
        except subprocess.CalledProcessError as e:
            print(f"{index}:命令执行失败: {res.json()}:{fn}")
            print(f"错误信息: {e}")
        except Exception as e:
            print(f"{index}:命令执行失败: {fn}")
            print(f"错误信息: {e}")
    return root_path
if __name__=='__main__':
    st.set_page_config(layout="wide",initial_sidebar_state ='collapsed')
    ### 关闭代理
    close_agent='unset all_proxy no_proxy http_proxy https_proxy'
    ### 打开代理
    open_agent='eval $(curl -s http://deploy.i.basemind.com/httpproxy)'
    command = "unset all_proxy no_proxy http_proxy https_proxy"
    completed_process =subprocess.run(command, shell=True)
    # 获取输出
    stdout = completed_process.stdout
    stderr = completed_process.stderr
    # 打印输出
    audio_oss_save='s3://libingx-data/AIGC/tts/'

    task_all=[]
    tos_dir_list=[f'/data/a10/lyw/dir_for_audio_slice/lq/{now}',f'/data/a10/melody/tts/lq/{now}']
    method_list=['whisper','funasr']
    one_col=st.columns([5,1])
    one_col_=one_col[0].columns([1,1])
    with one_col[0],one_col[1],one_col_[0],one_col_[1]:
        #select_tos_dir=one_col_[0].selectbox("火山云目录",tos_dir_list).strip()
        select_tos_dir=f'/data/a10/lyw/dir_for_audio_slice/lq/{now}'
        #select_method=one_col_[1].selectbox("whisper/funasr",method_list).strip()
        select_method=type_model
        #work=one_col[1].text_input('Work',4)
        #audio_path=one_col[0].text_input('本地OSS 音频目录','').strip()
        audio_path=dir_name
        #input_name=os.path.basename(audio_path)
        one_col[1].title('')
        basemind_oss_path=one_col[0].text_input('保存到basemind OSS地址',"").strip()
        one_col[1].title('')
        run_button=one_col[1].button('开始')
        audio_path=get_unquote(audio_path).replace("https://oss.iap.platform.basemind.com/", 's3://')
        audio_path=os.path.normpath(audio_path)
            
        basemind_oss_path=get_unquote(basemind_oss_path).replace("https://oss.iap.platform.basemind.com/", 's3://')
        #select_tos_dir=get_unquote(select_tos_dir).replace("https://oss.iap.platform.basemind.com/", 's3://')
        
        #if run_button:
        basename=''
        select_tos_dir=os.path.join(select_tos_dir,select_method)
        select_method_type=select_method
        if os.path.isdir(audio_path):
            basename=os.path.basename(audio_path)
            for root,dir,files in refile.smart_walk(audio_path):
                for file in files:
                    if file.endswith('.mp3') or file.endswith('.wav') or file.endswith('.m4a') :
                        output_dir_name=basename+'/'+file.rsplit('.',1)[0]
                        task_all.append((refile.smart_path_join(select_tos_dir,output_dir_name),select_method,refile.smart_path_join(root,file)))
                        #run_job(refile.smart_path_join(select_tos_dir,output_dir_name),select_method,refile.smart_path_join(root,file))
            with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
                futures=[executor.submit(run_job,task[0],task[1],task[2])
                        for task in task_all
                        ]
                executor.shutdown(wait=True)  # 等待所有任务完成
            for future in futures:
                one_col[0].write(f"火山云输出地址：{future.result() }")
        else:
            output_dir_name='/'+audio_path.rsplit('.',1)[0].split('/')[-1]
            root_path=run_job(refile.smart_path_join(select_tos_dir,output_dir_name),select_method,audio_path)
            one_col[0].write(f"火山云输出地址：{root_path}")
        end_time=time.time()
        execution_time= (end_time - start_time)/60
        one_col[0].write(f"耗时: {execution_time:.4f} 分")
        if select_method=="":
            select_method="whisper"
        one_col[0].write(f"火山云执行:  python3 /data/a10/lyw/dir_for_audio_slice/lq/tos_to_basemind.py --local {os.path.join(select_tos_dir,basename)} --type {select_method}")
        print(f"耗时: {execution_time:.4f} 分")