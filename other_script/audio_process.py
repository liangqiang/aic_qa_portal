import librosa
import os
import tarfile
import stat
import tempfile
import refile




def check_empty_audio(audio_file):
    # 读取音频文件
    audio_file = 'your_audio_file.wav'
    y, sr = librosa.load(audio_file)

    # 计算音频的能量
    energy = sum(y**2)

    # 设置一个阈值，用于判断音频是否为空
    threshold = 0.001  # 根据需要调整阈值
    return energy < threshold

def file_process(path):
    rm_file_list=[]
    rm_txt_count,rm_audio_count=0,0
    for root,dirs,files in os.walk(path):
        for file in files:
            if ".txt" in file:
                path_txt=os.path.join(root,file)
                with open(path_txt,'r') as f:
                    txt=f.read().strip().replace('[ZH]', '')
                    #print(txt)
                    if not txt:
                        rm_txt_count+=1
                        rm_file_list.append(path_txt)
                        rm_file_list.append(path_txt.replace('.txt', '.wav'))
            elif '.wav' in file:
                audio_file=os.path.join(root,file)
                # y, sr = librosa.load(audio_file)
                # energy = sum(y**2)
                if check_empty_audio(audio_file):
                    rm_audio_count+=1
                    rm_file_list.append(audio_file)
                    rm_file_list.append(audio_file.replace('.wav', '.txt'))
            for i in rm_file_list:
                os.remove(i)
    return rm_txt_count,rm_audio_count,set(rm_file_list)

if '__main__' ==__name__:
    path=r''
    rm_txt_count,rm_audio_count,rm_file_list=file_process(path)
    