import io
import os
import pandas as pd
import shutil
from datetime import datetime
import subprocess
import argparse
import shlex
from pydub import AudioSegment
from tqdm import tqdm
import numpy as np

import concurrent.futures
now = datetime.now().strftime("%Y%m%d")
def check_empty_audio(audio_path):
    try:
        if os.path.isfile(audio_path):
            audio = AudioSegment.from_file(audio_path)
        else :
            with open(audio_path, 'rb') as f:
                audio_data = f.read()
            audio = AudioSegment.from_file(io.BytesIO(audio_data))
        y = np.array(audio.get_array_of_samples())
        energy = sum(abs(y)**2)
    except Exception as e:
        print(f"{audio_path}:{e}")
        return True
        # 设置一个阈值，用于判断音频是否为空
    threshold = 0.0001  # 根据需要调整阈值
    return energy ==0,audio_path

def audio_process(path):
    all_task=[]
    rm_file_list=[]
    rm_audio_count=0
    for root,dirs,files in os.walk(path):
        for file in files:
            if file.endswith(".txt") :
                path_txt=os.path.join(root,file)
                with open(path_txt,'r') as f:
                    txt=f.read().strip().replace('[ZH]', '')
                    if not txt:
                        #os.path.s
                        rm_file_list.append(path_txt)
                        rm_file_list.append(path_txt.replace('.txt', '.wav'))
            elif file.endswith(('.wav','.mp3','.m4a'))  :
                audio_file=os.path.join(root,file)
                all_task.append(audio_file)

    with concurrent.futures.ThreadPoolExecutor(max_workers=int(8)) as executor: 
        futures=[executor.submit(check_empty_audio,task)for task in all_task]
    for future in tqdm(concurrent.futures.as_completed(futures)):
        flag,file=future.result()
        if flag:
            rm_file_list.append(file)
            rm_file_list.append(file.replace("."+file.rsplit('.')[-1], '.txt'))
    for file in set(rm_file_list):
        rm_audio_count+=1
        os.remove(file)
    return rm_audio_count
def read_txt(path_txt):
    with open(path_txt,'r',encoding='utf-8') as f:
        data=f.read()
    return data.strip()

def get_lable_excel(path):
    all_wav,all_txt,all_wav_path=[],[],[]
    df=pd.DataFrame()
    for root ,dirs,files in os.walk(path):
        for file in files:
            if file.endswith(('.wav','.m4a','.mp3')):
                _,suffix=os.path.splitext(file)
                #print(suffix)
                all_wav.append(file)
                all_txt.append(os.path.join(root,file.replace(f"{suffix}",'.txt')))
                all_wav_path.append(os.path.join(root,file))
    df['wav']=all_wav
    df['txt']=all_txt
    df['path']=all_wav_path
    df['txt']=df['txt'].apply(read_txt)
    basename=os.path.basename(path)
    df = df.sort_values(by='wav')
    df.to_excel(os.path.join(path,f'{basename}.xlsx'),index=False)
    
def process_audio_dir(path):
    type_flag=[]
    creat_dir_list=[]
    basename=os.path.basename(path)
    dir_list=os.listdir(path)
    tag_file_list=[]
    emporty_audio_list=[]
    for root,dirs,files in os.walk(path): 
        ### 删除空音频目录
        if  root.endswith('output'):
            file_list=os.listdir(root)
            if not bool({'whisper','funasr','faster_whisper'} & set(file_list)):
                pre,_=os.path.splitext(root)
                shutil.rmtree(pre)
                emporty_audio_list.append(pre)
        ### 处理音频
        elif  root.endswith(('whisper','funasr','faster_whisper')): 
            file_list=os.listdir(root)
            if 'slice' in file_list:
                tag_name=os.path.basename(root)
                json_file=[i for i in file_list if i.endswith('.json')][0]
                slice_path=os.path.join(root,'slice')
                shutil.move(os.path.join(root,json_file),os.path.join(slice_path,json_file))
                new_path=slice_path.split('/output')[0]+f"_{tag_name}"
                shutil.move(slice_path,new_path)
                type_flag.append(tag_name)
                tag_file_list.append({new_path:f"{now}_{tag_name}"})

    if type_flag:
        for i in set(type_flag):
            creat_dir=os.path.join(path,f"{basename}_{now}_{i}_slice_row")
            os.makedirs(creat_dir)
            creat_dir_list.append(creat_dir)
        for creat_dir in creat_dir_list :
            for tag in tag_file_list:
                for k,v in tag.items():
                    if v in creat_dir :
                        r_name=os.path.basename(k)
                        shutil.move(k,os.path.join(creat_dir,r_name))
        else:
            for i in dir_list:
                shutil.rmtree(os.path.join(path,i))
            emporty_audio_count=audio_process(path)
            print(f'删除空音频：{emporty_audio_count}/对')

    return creat_dir_list,emporty_audio_list

if __name__ == '__main__':
    #BASE_TOS=''
    parser = argparse.ArgumentParser()
    parser.add_argument('--local',required=True,type=str,help="input audio folder")
    parser.add_argument('--tos',type=str,default="aic-share/tts/denoise/",help="output tos ")
    parser.add_argument('--oss',type=str,default="libingx-data/lq/test/",help="output oss")
    
    args=parser.parse_args()
    
    basename=os.path.basename(args.local)
    tos_path=os.path.join(args.tos,f"{now}/{basename}")
    basemind_path=os.path.join(args.oss,f"{now}/{basename}")
    loacl_to_tos_cmd=f"rclone sync -P {shlex.quote(args.local)} volces-tos:{shlex.quote(tos_path)}"
    tos_to_oss_cmd=f'rclone sync -P  volces-tos:{shlex.quote(tos_path)}    basemind-oss:{shlex.quote(basemind_path)}'
    creat_list,emporty_audio_list=process_audio_dir(args.local)
    if emporty_audio_list:
        emporty_num=len(emporty_audio_list)
        print(f'{emporty_num}个目录无音频')
        print(emporty_audio_list)
    if creat_list:
        for creat in creat_list:
            get_lable_excel(creat)
        tos_result=subprocess.run(loacl_to_tos_cmd, shell=True,  text=True, check=True,stdout=subprocess.PIPE)
        print(tos_result.stdout)
        #oss_result=subprocess.run(tos_to_oss_cmd, shell=True,  text=True, check=True,stdout=subprocess.PIPE)
        #print(tos_to_oss_cmd.stdout)
        print("TOS PATH:",f"s3://{tos_path}")
        print("请在w2上传OSS 命令:",tos_to_oss_cmd)
    else:
        print('所有目录无切片音频')
